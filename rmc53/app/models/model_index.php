<?php if (!defined("INBOX")) die('separate call');

class Model_Index extends Model {

	public function index($var) {

		$params=array(
			'response'=>array(
				'order'=>'order',
				'direction'=>'asc',
				'limit'=>20,
				'offset'=>0
			),
			'type'=>1,
			'status'=>1
		);
		$var["banner"]=Banner::read($params);

		$params=array(
			'response'=>array(
				'type'=>'list',//list_all
				'text_preview'=>255,
				'status'=>'active',
				'order'=>'create',
				'direction'=>'desc',
				'limit'=>6,
				'offset'=>0
			),
			'cat_id'=>2
		);
		$var["article"]=Article::read($params);

		return $var;
	}


}
