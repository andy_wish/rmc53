<?php if (!defined("INBOX")) die('separate call');

class Model_Poll extends Model {

	public function fill($var) {

		if(!isset($var["post"])) Core::error_404();
		if(!isset($var["post"]["poll_id"])) Core::error_404();

		//echo '<pre>';var_dump($var["post"]);
		$var["page"]["title"]=DB::getOne('SELECT `name` FROM `poll` WHERE `id`=?i LIMIT 1', $var["post"]["poll_id"]);
		$var["page"]["description"]=$var["page"]["title"];
		$var["page"]["keywords"]=$var["page"]["title"];


		if(!isset($var["post"]["t"]) OR $var["post"]["t"]=='') $var["post"]["t"]=time();
		if(!isset($var["post"]["s"]) OR $var["post"]["s"]=='') $var["post"]["s"]=time();

		$fields=[];
		foreach($var["post"] as $name=>$value){
			$p=strpos($name, 'field_');
			if($p===0) {
				$id=substr($name, 6);
				$fields[]=['id'=>$id, 'value'=>$value];
			}
		}

		//var_dump($fields);die();
		//var_dump($_POST);
		//var_dump($_FILES);exit;
		
		if(isset($_FILES)){
			foreach($_FILES as $field_id=>$file){
				$result=Files::upload($file, 'poll');
				if(!isset($result["error"])) {
					$id=substr($field_id, 6);
					$fields[]=['id'=>$id, 'value'=>$result["src"].' <a href="/files/'.$result["type"].'/'.$result["src"].'"><i class="far fa-arrow-alt-circle-down"></i></a>'];
					//$result=Banner::create(array("image"=> $result["src"]));
				}
			}
		}

		$params=[
			"id"=>(int)$var["post"]["poll_id"],
			"start"=>(int)$var["post"]["s"],
			"tag"=>$var["post"]["t"],
			"fields"=>$fields
		];

		$result=Poll::fill($params);
		if(isset($result["error"])) {
			$var["text"]='<div class="alert alert-danger" role="alert"><span title="'.$result["error"].'">Ошибка. Что-то сломалось, мы уже работаем над этим.</span></div>';
			return $var;
		}
		$var["text"]='<div class="alert alert-success" role="alert"><span title="'.$result["total"].'">Отправлено, благодарим за внимание</span></div>';

		return $var;
	}


}
