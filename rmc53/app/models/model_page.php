<?php if (!defined("INBOX")) die('separate call');

class Model_Page extends Model {

	public function kontakty($var) {

		$params=[
			'response'=>[
				'status'=>'active'
			],
			'id'=>1
		];
		$var["article"]=Page::read($params);
		$var["page"]["title"]=$var["article"]["list"][0]["name"];
		$var["page"]["description"]=$var["article"]["list"][0]["description"];
		$var["page"]["keywords"]=$var["article"]["list"][0]["keywords"];

		return $var;
	}

	public function noko_progress($var) {

		$params=[
			'response'=>[
				'status'=>'active'
			],
			'id'=>3
		];
		$var["article"]=Page::read($params);
		$var["page"]["title"]=$var["article"]["list"][0]["name"];
		$var["page"]["description"]=$var["article"]["list"][0]["description"];
		$var["page"]["keywords"]=$var["article"]["list"][0]["keywords"];

		return $var;
	}
	
}
