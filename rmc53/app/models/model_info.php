<?php if (!defined("INBOX")) die('separate call');

class Model_Info extends Model {

	public function index($var) {
	}
	public function cat_list($var) {

		$params["response"]["type"]='one';
		$params["name_translit"]=$var["cat_name_translit"];
		$result=Category::read($params);
		$var["cat_id"]=$result["id"];
		$var["cat_name"]=$result["name"];

		
		$params["response"]["type"]='one';
		$params["name_translit"]=$var["cat_name_translit_parent"];
		$result=Category::read($params);
		$var["cat_id_parent"]=$result["id"];
		$var["cat_name_parent"]=$result["name"];
		

		$var["page"]["title"]=$var["cat_name"];
		$var["page"]["description"]=$var["cat_name"];
		$var["page"]["keywords"]=$var["cat_name"];



		/////PARAMS
		$params=array();
		$params["cat_id"]=$var["cat_id"];
			/////response
		$params["response"]["type"]='list';
		$params["response"]["text_preview"]=45;
		$params["response"]["status"]='active';
		//limit
		if(isset($var["get"]["limit"]) AND (int)$var["get"]["limit"]==$var["get"]["limit"] AND $var["get"]["limit"]>0 AND $var["get"]["limit"]<100) $params["response"]["limit"]=$var["get"]["limit"];
		else $params["response"]["limit"]=20;
		//offset
		if(isset($var["get"]["page"]) AND (int)$var["get"]["page"]==$var["get"]["page"] AND $var["get"]["page"]>0) {
			$var["get"]["page"]=$var["get"]["page"];
			$params["response"]["offset"]=($var["get"]["page"]-1)*$params["response"]["limit"];
			$var["pagination"]["current"]=(int)$var["get"]["page"];
		}else $var["pagination"]["current"]=1;
		//order
		if(isset($var["get"]["order"])) $params["response"]["order"]=$var["get"]["order"];
		//direction
		if(isset($var["get"]["dir"])) $params["response"]["direction"]=$var["get"]["dir"];

		$var["article"]=Article::read($params);

		////PAGINATION
		if($var["article"]["total_db"]>$params["response"]["limit"]){
			$var["pagination"]["finish"]=ceil($var["article"]["total_db"]/$params["response"]["limit"]);
			if($var["pagination"]["finish"]<=$var["pagination"]["current"]){
				$var["pagination"]["finish"]=false;
				$var["pagination"]["next"]=false;
			}else{
				$var["pagination"]["next"]=$var["pagination"]["current"]+1;
			}

			if($var["pagination"]["current"]>1) {
				$var["pagination"]["start"]=1;
				$var["pagination"]["prev"]=$var["pagination"]["current"]-1;
			}else {
				$var["pagination"]["start"]=false;
				$var["pagination"]["prev"]=false;
			}

			if($var["pagination"]["prev"] AND ($var["pagination"]["prev"]-1)>0) $var["pagination"]["prev2"]=$var["pagination"]["prev"]-1;
			else $var["pagination"]["prev2"]=false;

			if($var["pagination"]["next"] AND ($var["pagination"]["next"]+1)<$var["pagination"]["finish"]) $var["pagination"]["next2"]=$var["pagination"]["next"]+1;
			else $var["pagination"]["next2"]=false;
			//echo '<pre>';var_dump($var["pagination"]);
		}else $var["pagination"]["current"]=false;


		return $var;
	}

	public function article_read($var) {

		$params["response"]["type"]='one';
		$params["name_translit"]=$var["cat_name_translit"];
		$result=Category::read($params);
		if(!$result) Core::error_404();
		$var["cat_id"]=$result["id"];
		$var["cat_name"]=$result["name"];

		$params["response"]["type"]='one';
		$params["name_translit"]=$var["cat_name_translit_parent"];
		$result=Category::read($params);
		if(!$result) Core::error_404();
		$var["cat_id_parent"]=$result["id"];
		$var["cat_name_parent"]=$result["name"];

		$params=array(
			"name_translit"=>$var[1],
			"response"=>array(
				"limit"=>1,
				"type"=>'read'
			)
		);
		$var["article"]=Article::read($params);
		if(!$var["article"] OR $var["article"]["total"]<1) Core::error_404();


		$params=array(
			"type"=>1,
			"target_id"=>$var["article"]["list"][0]["id"]
		);
		$var["comment"]=Comment::read($params);

		$var["page"]["title"]=$var["article"]["list"][0]["name"];
		$var["page"]["description"]=$var["article"]["list"][0]["name"];
		$var["page"]["keywords"]=$var["article"]["list"][0]["name"];
		return $var;
	}

}
