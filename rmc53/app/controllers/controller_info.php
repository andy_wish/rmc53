<?php if (!defined("INBOX")) die('separate call');

class Controller_Info extends Controller {

	function __construct() {
		$this->model=new Model_Info();
		$this->view=new View();
	}

	function action_index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_article_list.php', $var);
	}


	function action_proektnyj_ofis($var) {
		$var["cat_name_translit"]=$var[0];
		$var["cat_name_translit_parent"]=$var["page"]["action"];

		if(!isset($var[1])) {//список статей
			$var=$this->model->cat_list($var);
			$this->view->generate('template.php', 'content_article_list.php', $var);
		}else{//вывод статьи
			$var=$this->model->article_read($var);
			$this->view->generate('template.php', 'content_article.php', $var);
		}
	}

	function action_meropriyatiya($var) {
		$var["cat_name_translit"]=$var[0];
		$var["cat_name_translit_parent"]=$var["page"]["action"];

		if(!isset($var[1])) {//список статей
			$var=$this->model->cat_list($var);
			$this->view->generate('template.php', 'content_article_list.php', $var);
		}else{//вывод статьи
			$var=$this->model->article_read($var);
			$this->view->generate('template.php', 'content_article.php', $var);
		}
	}

	function action_distancionnoe_obuchenie($var) {
		$var["cat_name_translit"]=$var[0];
		$var["cat_name_translit_parent"]=$var["page"]["action"];

		if(!isset($var[1])) {//список статей
			$var=$this->model->cat_list($var);
			$this->view->generate('template.php', 'content_article_list.php', $var);
		}else{//вывод статьи
			$var=$this->model->article_read($var);
			$this->view->generate('template.php', 'content_article.php', $var);
		}
	}

	function action_metodicheskij_blok($var) {
		$var["cat_name_translit"]=$var[0];
		$var["cat_name_translit_parent"]=$var["page"]["action"];

		if(!isset($var[1])) {//список статей
			$var=$this->model->cat_list($var);
			$this->view->generate('template.php', 'content_article_list.php', $var);
		}else{//вывод статьи
			$var=$this->model->article_read($var);
			$this->view->generate('template.php', 'content_article.php', $var);
		}
	}

	function action_roditelyam($var) {
		$var["cat_name_translit"]=$var[0];
		$var["cat_name_translit_parent"]=$var["page"]["action"];

		if(!isset($var[1])) {//список статей
			$var=$this->model->cat_list($var);
			$this->view->generate('template.php', 'content_article_list.php', $var);
		}else{//вывод статьи
			$var=$this->model->article_read($var);
			$this->view->generate('template.php', 'content_article.php', $var);
		}
	}
	
	function action_nezavisimaya_ocenka($var) {
		$var["cat_name_translit"]=$var[0];
		$var["cat_name_translit_parent"]=$var["page"]["action"];

		if(!isset($var[1])) {//список статей
			$var=$this->model->cat_list($var);
			$this->view->generate('template.php', 'content_article_list.php', $var);
		}else{//вывод статьи
			$var=$this->model->article_read($var);
			$this->view->generate('template.php', 'content_article.php', $var);
		}
	}
	
}
