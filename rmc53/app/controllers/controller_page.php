<?php if (!defined("INBOX")) die('separate call');

class Controller_Page extends Controller {

	function __construct() {
		$this->model = new Model_Page();
		$this->view = new View();
	}

	function action_kontakty($var) {
		$var=$this->model->kontakty($var);
		$this->view->generate('template.php', 'content_page_kontakty.php', $var);
	}

	function action_noko_progress($var) {
		$var=$this->model->noko_progress($var);
		$this->view->generate('template.php', 'content_page_noko_progress.php', $var);
	}

}
