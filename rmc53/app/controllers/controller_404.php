<?php if (!defined("INBOX")) die('separate call');

class Controller_404 extends Controller {

	function action_index($var) {
		$var["page_current"]='404';
		$var["page_title"]='Страница не найдена';
		$var["page_keywords"]='404';
		
		$this->view->generate('template.php', 'content_404.php', $var);
	}

}
