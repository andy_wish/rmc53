<?php if (!defined("INBOX")) die('separate call');

class Controller_Poll extends Controller {

	function __construct() {
		$this->model = new Model_Poll();
		$this->view = new View();
	}

	function action_fill($var) {
		$var=$this->model->fill($var);
		$this->view->generate('template.php', 'content_poll_result.php', $var);
	}

}
