<?php if (!defined("INBOX")) die('not allowed');

class View {

	function generate($template_file, $content_file, $var) {
		include 'app/views/'.$template_file;
	}

}