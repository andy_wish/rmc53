<?php if (!defined("INBOX")) die('separate call');

class Core {

	public static function get_header_data($var){
		$var["page"]["current"]='index';
		$var["page"]["title"]='Региональный модельный центр дополнительного образования детей Новгородской области';
		$var["page"]["description"]='Региональный модельный центр дополнительного образования детей Новгородской области';
		$var["page"]["keywords"]='модельный центр, дополнительное образования детей';

		require_once ROOT_DIR.'/library/category.php';
		$params["response"]["type"]='all';
		$var["category"]["all"]=Category::read($params);

		return $var;
	}

	public static function get_footer_data($var){

		return $var;
	}

	public static function error_404() {
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
		header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
	}

}