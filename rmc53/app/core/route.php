<?php if (!defined("INBOX")) die('separate call');

class Route {

	static function start() {

		$var=array();
		$var=self::environments($var);

		$controller_name='index';
		$action_name='index';

		$routes=explode('?', $_SERVER['REQUEST_URI']);
		$var["page"]["url"]=$routes[0];
		$routes=explode('/', $routes[0]);

		if (!empty($routes[1])) {
			$controller_name=$routes[1];
			if (!empty($routes[2])) {
				$action_name=$routes[2];
				if (isset($routes[3]) AND $routes[3]!='') {
					$var[0]=$routes[3];
					if (isset($routes[4]) AND $routes[4]!='') {
						$var[1]=$routes[4];
						if (isset($routes[5]) AND $routes[5]!='') {
							$var[2]=$routes[5];
						}
					}
				}
			}
		}

		if(!Access::permit($controller_name, $action_name)){
			$controller_name='unauthorized';
			$action_name='index';
			$var["page"]["url"]='unauthorized';
		}
		
		$var["page"]["controller"]=$controller_name;
		$var["page"]["action"]=$action_name;

		$var=Core::get_header_data($var);
		$var=Core::get_footer_data($var);

		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;

		$controller_path = 'app/controllers/'.strtolower($controller_name).'.php';
		if(file_exists($controller_path)) {
			include $controller_path;
			$model_path = 'app/models/'.strtolower($model_name).'.php';
			if(file_exists($model_path)) include $model_path;
		}else Core::error_404();

		$controller=new $controller_name;
		$action=$action_name;

		if(method_exists($controller, $action)) $controller->$action($var);
		else Core::error_404();
	}

	private static function environments($var){

		
		if(!empty($_POST)){
			foreach($_POST as $name => $value){
				$var["post"]["$name"]=$value;
			}
		}
		if(!empty($_GET)){
			foreach($_GET as $name => $value){
				$var["get"]["$name"]=$value;
			}
		}
		if(!empty($_COOKIE)){
			foreach($_COOKIE as $name => $value){
				$var["cookie"]["$name"]=$value;
			}
		}
		return $var;

	}


}