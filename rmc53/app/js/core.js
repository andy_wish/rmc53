
$(document).ready(function(){
	$('#login_trigger').click(function(){//login form toggle
		$('#login_content').slideToggle();
		$('#login_trigger').toggleClass('active');
		$('#user_email_login').focus();

		if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
		else $(this).find('span').html('&#x25BC;')
	});
		
});

function feedback_show(name='', email='', phone=''){
	modal_clear('top')
	$('#top_modal_header').append(
		$('<h4>Обратная связь</h4>'),
	);

	$('#top_modal_body').append(
		$('<input type="hidden" id="feedback_user_r" value="valid_user_false" />'),
		$('<div class="form-group">').append(
			$('<label for="feedback_name">*Как к Вам обращаться?</label>'),
			$('<input id="feedback_name" type="text" class="form-control form-control-sm" aria-describedby="feedback_name" placeholder="Имя" required value="'+name+'" />'),
		),
		$('<div class="form-group">').append(
			$('<label for="feedback_text">*Вопрос или пожелание</label>'),
			$('<textarea id="feedback_text" class="form-control form-control-sm" placeholder="Текст" required></textarea>'),
		),
		$('<div class="form-group">').append(
			$('<label for="feedback_email">*email для связи с Вами</label>'),
			$('<input id="feedback_email" type="text" class="form-control form-control-sm" aria-describedby="feedback_email" placeholder="email" required value="'+email+'" />'),
		),
		$('<div class="form-group">').append(
			$('<label for="feedback_phone">телефон, не обязательно</label>'),
			$('<input id="feedback_phone" type="text" class="form-control form-control-sm" aria-describedby="feedback_phone" placeholder="телефон" value="'+phone+'" />'),
		),
		$('<div class="form-check mb-2 mr-sm-2 mb-sm-0">').append(
			$('<label class="form-check-label">').append(
				$('<input class="form-check-input" id="feedback_check" type="checkbox" />'),
				$('<span style="padding-bottom: .15rem">Я не робот</span>'),
			),
		),
		$('<div id="modal_info_box">'),
	);

	$('#top_modal_footer').append(
		$('<div class="btn-group" role="group" aria-label="service_info_buttons">').append(
			$('<button class="btn btn-success btn-block" OnClick="feedback_submit()" disabled id="feedback_btn_submit" >Отправить</button>'),
			$('<button class="btn btn-danger" OnClick="modal_close(\'top\')">Отмена</button>'),
		),
	);
	
	$('#feedback_check').change(function() {
		if($(this).is(':checked')){
			$("#feedback_btn_submit").removeAttr('disabled');
			$('#feedback_user_r').val('valid_user_true');
		}else {
			$("#feedback_btn_submit").attr('disabled','disabled');
			$('#feedback_user_r').val('valid_user_false');
		}
	});
	
	modal_show('top', '30rem')
}
function feedback_submit() {

	var feedback_name=$("#feedback_name").val();
	var feedback_text=$("#feedback_text").val();
	var feedback_email=$("#feedback_email").val();
	var feedback_phone=$("#feedback_phone").val();
	var feedback_user_r=$("#feedback_user_r").val();

	if(feedback_name.length<3 || feedback_name.length>50) {
		$("#modal_info_box").html('<div class="alert alert-danger" role="alert">Имя обязательно для заполнения, минимум 3 символа</div>');
		return false;
	}
	if(feedback_text.length<3 || feedback_text.length>255) {
		$("#modal_info_box").html('<div class="alert alert-danger" role="alert">Текст послания обязателен для заполнения, от 3 до 255 символов</div>');
		return false;
	}
	if(feedback_email.length<6 || feedback_email.length>80) {
		$("#modal_info_box").html('<div class="alert alert-danger" role="alert">email обязательно для заполнения, минимум 6 символов</div>');
		return false;
	}
	if(feedback_phone!='' && (feedback_phone.length<6 || feedback_phone.length>80)) {
		$("#modal_info_box").html('<div class="alert alert-danger" role="alert">номер телефона минимум 6 символов</div>');
		return false;
	}
	
	wait_start();
	
	var params={
		"name": feedback_name,
		"text": feedback_text,
		"email": feedback_email,
		"user_r": feedback_user_r
	};
	if(feedback_phone!='') params["phone"]=feedback_phone;

	answer = jsonrpc_request("comment.create_feedback", params);
	if(typeof(answer["error"])!="undefined") {
		$("#modal_info_box").html('<div class="alert alert-danger" role="alert">'+answer["error"]["message"]+'</div>');
		wait_finish();
		return false;
	}
	if(answer["result"]["id"]>0) {
		modal_clear('top')
		$('#top_modal_body').append(
			$('<h2 class="alert alert-success">Отправлено, благодарим за внимание</h2>'),
		);
		$('#top_modal_footer').append(
			$('<button class="btn btn-secondary btn-block" OnClick="modal_close(\'top\')">Закрыть</button>'),
		);
	}else {
		$("#modal_info_box").html('<div class="alert alert-danger" role="alert">Ошибка. Попробуйте позднее</div>');
		wait_finish();
		return false;
	}
	wait_finish();
}
function wait_start(){
	$('#wait_notify').fadeIn();
}
function wait_finish(result=1, desc=''){
	$('#wait_notify').fadeOut();
}

function modal_clear(position){
	$('#'+position+'_modal_header').empty();
	$('#'+position+'_modal_body').empty();
	$('#'+position+'_modal_footer').empty();
}
function modal_show(position, width='60rem'){
	$('.modal_box').css('max-width', width);
	$('#'+position+'_modal').modal();
}
function modal_close(position){
	$('#'+position+'_modal').modal('hide');
}

function setCookie(n,v,h) {var d = new Date();d.setTime(d.getTime() + (h*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=n+"="+v+";"+expires+";path=/";}
function getCookie(n) {var n=n+"=";var decodedCookie=decodeURIComponent(document.cookie);var ca=decodedCookie.split(';');for(var i=0;i<ca.length;i++) {var c=ca[i];while (c.charAt(0)==' '){c=c.substring(1);}if (c.indexOf(n)==0) {return c.substring(n.length,c.length);}}return "";}