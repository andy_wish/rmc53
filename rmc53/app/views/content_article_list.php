<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>

				<li class="breadcrumb-item active"><?=$var["cat_name_parent"]?> / <?=$var["cat_name"]?></li>
			</ol>
		</div>
	</div>
</div>

<div class="container article_list">


<div class="row my-3">
	<div class="сol-12"><h1><?=$var["page"]["title"]?></h1></div>
</div>

<?php
if($var["article"]["total"]>0){

	foreach($var["article"]["list"] as $row){
?>
<article class="row mb-4 justify-content-center">
	<div class="col-md-4 text-right">
<?php
		if($row["img_src"]!='')$img_src='/files/image/'.$row["img_src"];
		else $img_src='/image/no-img.png';
?>
		<a href="/info/<?=$row["cat_name_translit_parent"]?>/<?=$row["cat_name_translit"]?>/<?=$row["name_translit"]?>"><img src="<?=$img_src?>" alt="<?=$row["name"]?>" class="rounded event_img img-fluid" /></a>
<?php
		if($row["file_count"]>0){
?>
		<div class="text-muted" title="прикреплённые файлы">
			<small><?=$row["file_count"]?> <i class="far fa-file"></i></small>
		</div>
<?php
		}
?>
	</div>
	<div class="col-md-8">

		<time class="my-1 event_date" datetime="<?=$row["create"]?>">
			<?=$row["create_nice"]?>&emsp;<?=$row["rating"]!=''? '<span title="рейтинг"><small class="far fa-star text-muted"></small>'.$row["rating"].'</span>' : ''?>
		</time>

		<header class="event_name trans_200 my-1">
			<a href="/info/<?=$row["cat_name_translit_parent"]?>/<?=$row["cat_name_translit"]?>/<?=$row["name_translit"]?>">
				<h4><?=$row["name"]?></h4>
			</a>
		</header>

		<p><?=$row["text"]?></p>

		<footer>
			<a class="button text-center trans_200" href="/info/<?=$row["cat_name_translit_parent"]?>/<?=$row["cat_name_translit"]?>/<?=$row["name_translit"]?>">Подробно</a>
		</footer>

	</div>
</article>
<?php
	}
?>

<?php
	if($var["pagination"]["current"]){
?>
<div class="row my-3 justify-content-center">
	<div class="сol-12" id="pagination">
		<div class="btn-toolbar" role="toolbar" aria-label="Pagination">
<?php

		if($var["pagination"]["start"]) {
?>
			<div class="btn-group mr-2" role="group" aria-label="В начало страниц">
				<a href="<?=$var["page"]["url"]?>" class="btn btn-secondary">В начало</a>
			</div>
<?php
		}
?>
			<div class="btn-group mr-2" role="group" aria-label="">
<?php
		if($var["pagination"]["prev"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev"]?>" class="btn btn-secondary"><i class="fas fa-arrow-left"></i></a>
<?php
		}
		if($var["pagination"]["prev2"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev2"]?>" class="btn btn-secondary"><?=$var["pagination"]["prev2"]?></a>
<?php
		}
		if($var["pagination"]["prev"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev"]?>" class="btn btn-secondary"><?=$var["pagination"]["prev"]?></a>
<?php
		}
?>

				<span class="btn btn-warning pagination_current"><?=$var["pagination"]["current"]?></span>

<?php
		if($var["pagination"]["next"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next"]?>" class="btn btn-secondary"><?=$var["pagination"]["next"]?></a>
<?php
		}
		if($var["pagination"]["next2"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next2"]?>" class="btn btn-secondary"><?=$var["pagination"]["next2"]?></a>
<?php
		}
		if($var["pagination"]["next"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next"]?>" class="btn btn-secondary"><i class="fas fa-arrow-right"></i></a>
<?php
		}
?>
			</div>
<?php
		if($var["pagination"]["finish"]) {
?>
			<div class="btn-group mr-2" role="group" aria-label="В конец страниц">
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["finish"]?>" class="btn btn-secondary">В конец</a>
			</div>
<?php
		}
?>
		</div>
	</div>
</div>
	
<?php
	}
}
?>

</div>