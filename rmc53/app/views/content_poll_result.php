<div class="container article_read_wide" style="min-height:18rem">

	<!--TITLE-->
	<div class="row my-2">
		<div class="col-12">
			<h1><?=$var["page"]["title"]?></h1>
		</div>
	</div>


	<!--TEXT-->
	<div class="row my-4">
		<div class="col-12 article_text">
			<?=$var["text"]?>
		</div>
	</div>

</div>
