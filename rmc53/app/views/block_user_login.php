<?php if (!defined("INBOX")) die('separate call');

if(isset($_GET["auth_error"])){
	switch ($_GET["auth_error"]) {
		case 'user not found':
			$auth_error='Не найден';
			break;
		case 'login_error':
			$auth_error='Неправильные логин/пароль.';
			break;
		case 1:
			$auth_error='Ошибка ввода данных.';
			break;
		case 'logout_error':
			$auth_error='Ошибка выхода.';
			break;
		case 3:
			$auth_error='Ошибка. Подмена запроса.';
		default:
			$auth_error='Ошибка.';
	}
	echo '<span class="badge badge-danger">'.$auth_error.'</span>';
}

if(User::id()){
?>
<form action="/user/logout/" method="post" class="form-inline my-2 my-lg-0">
	<div class="input-group input-group-sm">
		<div class="input-group-append">
			<div class="input-group-text"><i class="fas fa-user-circle"></i> <strong><?=User::name()?></strong></div>
		</div>
		<div class="input-group-append">
			<button type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-sign-out-alt"></i> выйти</button>
		</div>
	</div>
</form>

<?php 
}else{
?>

	<button type="button" class="btn btn-secondary" id="login_trigger"><span>&#x25BC;</span> Войти</button>
	<a href="/user/registration/" class="btn btn-secondary"><i class="fas fa-pencil-alt"></i> Регистрация</a>

<div style="display:none" id="login_content">
	<form action="/user/login/" method="post">
		<input type="hidden" name="page_current" value="<?=$_SERVER['REQUEST_URI']?>" />
		<div class="form-group">
			<input name="user_email" id="user_email_login" type="email" class="form-control form-control-sm" placeholder="email" required />
		</div>
		<div class="form-group">
			<input name="user_password" type="password" class="form-control form-control-sm" placeholder="Password" required />
		</div>
		<button type="submit" class="btn btn-light btn-sm btn-block"><i class="fas fa-sign-in-alt"></i> вперёд</button>
	</form>
</div>                     

<?php } ?>