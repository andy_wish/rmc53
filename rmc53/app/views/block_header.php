<?php if (!defined("INBOX")) die('separate call');?>
<!DOCTYPE html>
<html lang="ru">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link rel="apple-touch-icon" sizes="180x180" href="/image/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="/image/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/image/favicon/favicon-16x16.png">
<link rel="icon" type="image/x-icon" href="/image/favicon/favicon.ico">
<link rel="manifest" href="/image/favicon/site.webmanifest">
<link rel="mask-icon" href="/image/favicon/safari-pinned-tab.svg" color="#00c1f7">
<link rel="shortcut icon" href="/image/favicon/favicon.ico">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-config" content="/image/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

<meta name="keywords" content="<?=isset($var["page"]["keywords"])? $var["page"]["keywords"] : 'Региональный модельный центр дополнительного образования детей Новгородской области'?>" />
<meta name="description" content="<?=isset($var["page"]["description"])? $var["page"]["description"] : 'Региональный модельный центр дополнительного образования детей Новгородской области'?>" />
<title><?=isset($var["page"]["title"])? $var["page"]["title"] : 'Региональный модельный центр дополнительного образования детей Новгородской области'?></title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="/plugins/bootstrap/bootstrap.min.css">
<link href="/plugins/fontawesome/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/plugins/owlcarousel/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/plugins/owlcarousel/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="/plugins/owlcarousel/animate.css">
<link rel="stylesheet" type="text/css" href="/app/css/style.css">
<link rel="stylesheet" type="text/css" href="/app/css/responsive.css">
<script src="/plugins/ulogin.js"></script>

<script src="/plugins/poll.js"></script>
<script src="/plugins/jquery-3.2.1.min.js"></script>
<script src="/plugins/popper.min.js"></script>
<script src="/plugins/bootstrap/bootstrap.min.js"></script>
<script src="/plugins/ScrollMagic.min.js"></script>
<script src="/plugins/owlcarousel/owl.carousel.js"></script>
<script src="/plugins/jsonrpc.js"></script>
<script src="/app/js/core.js"></script>
<script src="/app/js/search.js"></script>
<script src="//leadbox.umdi.ru/public/leadbox.umdi.js"></script>
<meta name="yandex-verification" content="23c5dc78530efaf1" />
</head>
<body style="background: url(/image/bg.jpg) repeat-y;background-size:100%;">

<header class="header container-fluid">

	<div class="row justify-content-around align-items-center">

		<div class="col-lg-1 col-md-2 col-sm-2 text-center">
			<a href="/" id="logo_img"><img class="mt-1 ml-2" src="/image/logo.png" alt="РМЦ53" height="70" width="70" /></a>
		</div>

		<div class="col-lg-4 col-md-6 col-sm-10 align-items-center">
			<div class="ml-2 text-left" id="logo_title">Региональный&nbsp;модельный&nbsp;центр<br />дополнительного&nbsp;образования&nbsp;детей<br />Новгородской&nbsp;области</div>
		</div>
		
		<div class="col-lg-7 col-md-12 col-sm-12 pt-lg-2 pb-md-2 pb-sm-2 pb-xs-2 text-center">
		
			<div class="input-group input-group-sm">

				<input id="search_box" class="form-control" type="text" autocomplete="off" placeholder="поиск" value="" maxlength="20" OnKeyUp="search_top_main(event.keyCode)" OnClick="search_top_main('click')" />
				<div class="input-group-append">
					<span class="input-group-text badge badge-secondary" id="search_count_box"></span>
				</div>
				<table class="table table-sm table-striped" id="search_list"></table>
				<div class="input-group-append">
					<button class="btn btn-sm btn-secondary" id="feedback_trigger" onClick="feedback_show('<?=User::id()? User::name() : ''?>', '<?=User::id()? User::email() : ''?>', '<?=User::id()? User::phone() : ''?>')"><i class="far fa-comment-alt"></i> Обратная связь</button>
				</div>

				<div class="input-group-append">
					<?php require 'block_user_login.php';?>
				</div>
			</div>
			
		</div>

	</div>


	<nav>
	<ul class="row nav justify-content-center nav-pills">
<?php
foreach($var["category"]["all"] as $row){

	if($row["name_translit"]==$var["page"]["action"]) $active_mark=' active';
	else $active_mark='';

?>
		<li class="nav-item">
<?php
	if($row["sub"]!=''){
?>
			<a class="nav-link dropdown-toggle<?=$active_mark?>" data-toggle="dropdown" href="/info/<?=$row["name_translit"]?>/"><?=$row["name"]?></a>
				<div class="dropdown-menu">
<?php
		foreach($row["sub"] as $sub_row){
			switch ($sub_row["sub_type"]){
				case 1://list
					$href='/info/'.$row["name_translit"].'/'.$sub_row["sub_name_translit"].'/';
					break;
				case 2://link
					$href=$sub_row["sub_desc"];
					break;
				case 3://text
					$href='/info/'.$row["name_translit"].'/'.$sub_row["sub_name_translit"].'/';
					break;
				default://list
					$href='/info/'.$row["name_translit"].'/'.$sub_row["sub_name_translit"].'/';
			}
?>
					<a class="dropdown-item nav_dropdown_item" href="<?=$href?>"><?=$sub_row["sub_name"]?></a>
<?php
		}
?>
				</div>
<?php
	}else{
?>
			<a class="nav-link<?=$active_mark?>" href="/info/<?=$row["name_translit"]?>/"><?=$row["name"]?></a>
<?php
	}
?>
		</li>
<?php
		}
?>
		<li class="nav-item">
			<a class="nav-link
<?php
		if($var["page"]["action"]=='kontakty') echo ' active';
?>
			" href="/page/kontakty/">Контакты</a>
		</li>
	
	</ul>
	</nav>

</header>

<!--MODAL-->
<div class="modal fade" id="top_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal_box" role="document">
		<div class="modal-content" id="top_modal_box">
			<button class="btn btn-sm btn-secondary m-1" style="position:absolute;right:0;" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
			<div class="modal-header pr-5" id="top_modal_header"></div>
			<div class="modal-body" id="top_modal_body"></div>
			<div class="modal-footer" id="top_modal_footer"></div>
		</div>
	</div>
</div>

<!--wait_notify-->
<div id="wait_notify" style="display:none;position:fixed;top:90%;left:90%;z-index:9999;opacity:.5;"><i class="fas fa-cog fa-5x fa-spin"></i></div>