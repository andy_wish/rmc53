<?php
if($var["banner"]["total"]>0){
?>
<div class="container-fluid">
	<div class="row">
<?php
	if($var["banner"]["total"]>0){
?>
	<div class="hero_slider owl-carousel">
<?php
		foreach($var["banner"]["list"] as $row){
?>
		<div class="hero_slide">
			<div class="hero_slide_background" style="background-image:url(/files/image/<?=$row["image"]?>)"></div>
			<div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
				<div class="hero_slide_content text-center">
					<?=$row["name"]!=''? '<div class="slider_name" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.$row["name"].'</div>' : ''?>
					<?=$row["text"]!=''? '<div class="slider_text" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.$row["text"].'</div>' : ''?>
					<?=$row["link"]!=''? '<a class="button slider_link m-3" href="'.$row["link"].'" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">перейти</a>' : ''?>
				</div>
			</div>
		</div>
<?php
		}
?>

	</div>
	<div class="hero_slider_left hero_slider_nav trans_200"><i class="fas fa-arrow-circle-left"></i></div>
	<div class="hero_slider_right hero_slider_nav trans_200"><i class="fas fa-arrow-circle-right"></i></div>
<?php
	}
?>
	</div>
</div>
<?php
}
?>

<?php
if($var["article"]["total"]>0){
?>
<div class="news_box container my-3">
	<div class="row">
		<h2 class="block_title">Последние новости</h2>
	</div>
	<div class="row">

<?php
	foreach($var["article"]["list"] as $row){
?>
		<div class="col-md-4">

			<div class="mx-2 my-1 event_date">
				<?=$row["create_nice"]?>
			</div>
		
			<div>
<?php
		if($row["img_src"]!='')$img_src='/files/image/'.$row["img_src"];
		else $img_src='/image/no-img.png';
?>
				<a href="/info/meropriyatiya/novosti/<?=$row["name_translit"]?>/"><img src="<?=$img_src?>" alt="<?=$row["name"]?>" class="rounded event_img img-fluid" /></a>
			</div>

			<div class="event_name trans_200 mx-2 my-1"><a href="/info/meropriyatiya/novosti/<?=$row["name_translit"]?>/"><?=$row["name"]?></a></div>
			<p><?=$row["text"]?></p>
		</div>
<?php
	}
?>
	</div>
</div>
<?php
}
?>
<script type="text/javascript">
$(document).ready(function(){

	initHeroSlider();

	/* Init Hero Slider*/
	function initHeroSlider(){
		if($('.hero_slider').length){
			var owl = $('.hero_slider');

			owl.owlCarousel({
				items:1,
				loop:true,
				smartSpeed:900,
				autoplay:true,
				nav:false,
				dots:false
			});

			// add animate.css class(es) to the elements to be animated
			function setAnimation ( _elem, _InOut ){
				// Store all animationend event name in a string.
				// cf animate.css documentation
				var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

				_elem.each ( function (){
					var $elem = $(this);
					var $animationType = 'animated ' + $elem.data( 'animation-' + _InOut );

					$elem.addClass($animationType).one(animationEndEvent, function (){
						$elem.removeClass($animationType); // remove animate.css Class at the end of the animations
					});
				});
			}

			// Fired before current slide change
			owl.on('change.owl.carousel', function(event){
				var $currentItem = $('.owl-item', owl).eq(event.item.index);
				var $elemsToanim = $currentItem.find("[data-animation-out]");
				setAnimation ($elemsToanim, 'out');
			});

			// Fired after current slide has been changed
			owl.on('changed.owl.carousel', function(event){
				var $currentItem = $('.owl-item', owl).eq(event.item.index);
				var $elemsToanim = $currentItem.find("[data-animation-in]");
				setAnimation ($elemsToanim, 'in');
			})

			// Handle Custom Navigation
			if($('.hero_slider_left').length){
				var owlPrev = $('.hero_slider_left');
				owlPrev.on('click', function(){
					owl.trigger('prev.owl.carousel');
				});
			}

			if($('.hero_slider_right').length){
				var owlNext = $('.hero_slider_right');
				owlNext.on('click', function(){
					owl.trigger('next.owl.carousel');
				});
			}
		}	
	}

});
</script>