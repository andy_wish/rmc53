<footer class="footer p-3 container-fluid">
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-5">
			<div class="footer_icon"><i class="far fa-copyright"></i></div>
				2018 Региональный модельный центр дополнительного образования детей Новгородской области
		</div>
		<div class="col-lg-5 col-md-4 col-sm-3">
			<span>Мы в социальных сетях</span><br />
			<a href="https://vk.com/club162992583"><img class="social_img" src="/image/vk.png" height="30" width="30" alt="vk" /></a>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4">
			<ul>
				<li class="mb-1">
					<div class="footer_icon"><i class="fas fa-graduation-cap"></i></div>
					ГОАУ ДПО <a href="http://dpo53.ru">«Региональный институт профессионального развития»</a>
				</li>
				<li class="mb-1">
					<div class="footer_icon"><i class="fas fa-money-check"></i></div>
					ОГРН 1025300791570 ИНН 5321062368
				</li>
				<li class="mb-1">
					<div class="footer_icon"><i class="fas fa-map-marker-alt"></i></div>
					Великий Новгород, ул. Новолучанская д. 27
				</li>
				<li class="mb-1">
					<div class="footer_icon"><i class="fa fa-phone"></i></div>
					+7 (8162) 77-14-63
				</li>
				<li class="mb-1">
					<div class="footer_icon"><i class="far fa-envelope"></i></div>
					<a href="mailto:mail@dpo53.ru?subject=Письмо-с-сайта-rmc53.ru">mail@dpo53.ru</a>
				</li>

			</ul>
		</div>
	</div>
</footer>
<!--Metrika--><script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter49900738 = new Ya.Metrika2({ id:49900738, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/tag.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks2"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/49900738" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
</body></html>