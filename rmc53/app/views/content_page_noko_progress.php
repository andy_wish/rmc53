<div class="container article_read_wide">

	<!--TITLE-->
	<div class="row my-2">
		<div class="col-12">
			<h1><?=$var["page"]["title"]?></h1>
		</div>
	</div>


	<!--TEXT-->
	<div class="row my-4">
		<div class="col-12 article_text">
			<?=$var["article"]["list"][0]["text"]?>
			<div class="mx-1 mb-2">
				<span class="text-muted">Организации</span>
				<div class="input-group">
					<select class="form-control" id="leadbox_umdi_report_select" size="6" onChange="leadbox_umdi_report_read()"></select>
				</div>
			</div>
			<div id="leadbox_umdi_report_progress" style="min-height:20rem"></div>
			
		</div>
	</div>
	
	
	
	

</div>
<script type="text/javascript">

var leadbox_umdi_current={"organization": false, "count": 0}

var leadbox_umdi_noko_organizations={
	"ГОАУ ДО «Морской центр капитана Варухина Н.Г.»": 650,
	"МАУ ДО «Батецкая школа искусств»": 36,
	"МАУ ДО «Центр дополнительного образования детей п. Батецкий»": 213,
	"МАУ ДО «Центр дополнительного образования для детей п. Волот»": 50,
	"МБУ ДО «Волотовская детская школа искусств»": 10000,
	"МАУ ДО «Центр дополнительного образования» с. Марево": 149,
	"МБУ ДО «Детская школа искусств» с. Мошенское": 141,
	"МАУ ДО «Детско-юношеская спортивная школа» г. Окуловка с филиалами": 556,
	"МБУ ДО «Детская музыкальная школа имени Н. А. Римского-Корсакова» г. Окуловка с филиалами": 10000,
	"МБУ ДО  «Детская музыкальная школа» п. Угловка": 10000,
	"МАУ ДО  «Поддорская музыкальная школа»": 70,
	"МАУ ДО «Центр внешкольной работы Новгородского муниципального района»": 2500,
	"МБУ ДО  «Борковская детская школа искусств»": 107,
	"МБУ ДО  «Пролетарская детская школа искусств»": 10000,
	"МБУ ДО  «Детская школа искусств» д. Ермолино": 10000,
	"МБУ ДО  «Детская школа искусств» д. Чечулино": 50,
	"МАУ ДО «Центр внешкольной работы» г. Пестово": 100,
	"МБУ ДО «Спортивная школа» г. Пестово": 10000,
	"МБУ ДО «Пестовская детская школа искусств»": 262,
	"МАУ ДО «Центр дополнительного образования» г. Холм": 160,
	"МБУ ДО «Холмская детская школа искусств»": 10000,
	"МАУ ДО «Центр дополнительного образования детей» п. Шимск": 315,
	"МБУ ДО «Шимская детская школа искусств»": 10000
}

$(document).ready(function() {
	$.each(leadbox_umdi_noko_organizations, function (i, val) {
		$('#leadbox_umdi_report_select').append(
			$('<option value="'+i+'">'+i+'</option>')
		);

	})
})

function leadbox_umdi_report_read(){
	document.getElementById('leadbox_umdi_report_progress').innerHTML='';
	if($('#leadbox_umdi_report_select').val()!=''){
		leadbox_umdi_current["organization"]=$('#leadbox_umdi_report_select').val()
		var params={
			"tag": 'noko_fill_progress',
			"response":{"organization_name": leadbox_umdi_current["organization"]}
		}
	}else{
		leadbox_umdi_current["organization"]=false
		return false
	}

	wait_start()

	var leadbox_umdi_answer=leadbox_umdi_request('report.read', params)
	var progress=''
	
	if(typeof(leadbox_umdi_answer["result"])!="undefined" && typeof(leadbox_umdi_answer["result"]["error"])!="undefined"){
		if(leadbox_umdi_answer["result"]["error"]=='boxes not found'){
			var error_out='Анкеты не найдены'
		}else{
			var error_out='Ошибка: '+leadbox_umdi_answer["result"]["error"]
		}
		$("#leadbox_umdi_report_progress").append(
			$('<h4>'+error_out+'</h4>')
		)
		console.log(leadbox_umdi_answer["result"]["error"])
		wait_finish()
		return false
	}else {
	
		if(
			typeof leadbox_umdi_noko_organizations[leadbox_umdi_current["organization"]] !=='undefined' &&
			typeof leadbox_umdi_answer["result"]["total_box"][4] !=='undefined'
		) {
			var percent=leadbox_umdi_noko_organizations[leadbox_umdi_current["organization"]]/100
			percent=leadbox_umdi_answer["result"]["total_box"][4]/percent
			percent=Math.floor(percent)
			if(percent<50) var bg_progress='bg-danger'
			else if(percent<80) var bg_progress='bg-warning'
			else var bg_progress='bg-success'
			
			progress='<div class="progress" title="всего: '+leadbox_umdi_noko_organizations[leadbox_umdi_current["organization"]]+'"><div class="progress-bar '+bg_progress+'" role="progressbar" style="width: '+percent+'%;" aria-valuenow="'+percent+'" aria-valuemin="0" aria-valuemax="'+leadbox_umdi_noko_organizations[leadbox_umdi_current["organization"]]+'">'+percent+'%</div></div>'
		}

		$("#leadbox_umdi_report_progress").append(
			$('<h4>Всего анкет заполнено: '+leadbox_umdi_answer["result"]["total"]+'</h4>'),
			$('<table class="table" id="leadbox_umdi_report_progress_table">').append(
				$('<tr>').append(
					$('<td>Родители: <strong>'+leadbox_umdi_answer["result"]["total_box"][3]+'</strong></td>'),
					$('<td>').append(
						$('<span>Дети: <strong>'+leadbox_umdi_answer["result"]["total_box"][4]+'</strong></span>'),
						$(progress)
					)
				),
				$('<tr>').append(
					$('<td id="leadbox_umdi_report_progress_3">'),
					$('<td id="leadbox_umdi_report_progress_4">')
				)
			)
		)
		if(typeof(leadbox_umdi_answer["result"]["directions"][3])!=='undefined'){
			$.each(leadbox_umdi_answer["result"]["directions"][3], function(i, val) {
				//console.log(this)
				$("#leadbox_umdi_report_progress_3").append('<span>'+i+': <strong>'+val+'</strong></span><br />')
			})
		}
		if(typeof(leadbox_umdi_answer["result"]["directions"][4])!=='undefined'){
			$.each(leadbox_umdi_answer["result"]["directions"][4], function(i, val) {
				//console.log(this)
				$("#leadbox_umdi_report_progress_4").append('<span>'+i+': <strong>'+val+'</strong></span><br />')
			})
		}
		wait_finish()
	}
}


</script>