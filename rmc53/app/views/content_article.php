<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/">Главная</a></li>
				<li class="breadcrumb-item"><a href="/info/<?=$var["cat_name_translit_parent"]?>/<?=$var["cat_name_translit"]?>/"><?=$var["cat_name_parent"]?> / <?=$var["cat_name"]?></a></li>
<?php
if($var["article"]["total"]>0){
?>
				<li class="breadcrumb-item active"><?=$var["page"]["title"]?></li>
<?php
}
?>
			</ol>
		</div>
	</div>
</div>

<?php
//echo '<pre>';var_dump($var["article"]);
if($var["article"]["total"]>0){
?>



<article class="container article_read">

	<!--TITLE-->
	<header class="row mb-1">
		<div class="col-12">
			<time class="my-1 event_date" datetime="<?=$var["article"]["list"][0]["create"]?>">
				<?=$var["article"]["list"][0]["create_nice"]?>&emsp;<?=$var["article"]["list"][0]["rating"]!=''? '<span title="рейтинг"><small class="far fa-star text-muted"></small>'.$var["article"]["list"][0]["rating"].'</span>' : ''?>
			</time>
			<!--<time datetime="2018-09-30T18:24:53" pubdate>30 Сентября</time>-->
			<div class="сol-12"><h1><?=$var["page"]["title"]?></h1></div>
		</div>
	</header>

	<!--IMAGE-->
	<div class="row">
		<div class="col-12 text-center">
<?php
		//SLIDER
	if(isset($var["article"]["list"][0]["images"]) AND count($var["article"]["list"][0]["images"])>1){
?>
			<div class="hero_slider owl-carousel">
<?php
		foreach($var["article"]["list"][0]["images"] as $row){
?>
				<div class="hero_slide">
					<div class="hero_slide_background" style="background-image:url(/files/<?=$row["type"]?>/<?=$row["src"]?>)"></div>
					<div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
						<div class="hero_slide_content text-center">
							<?=$row["name"]!=''? '<div class="slider_name" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.$row["name"].'</div>' : ''?>
							<?=$row["desc"]!=''? '<div class="slider_text" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">'.$row["desc"].'</div>' : ''?>
						</div>
					</div>
				</div>
<?php
		}
?>
			</div>
<?php
	}elseif($var["article"]["list"][0]["img_src"]!=''){
?>
			<img src="/files/image/<?=$var["article"]["list"][0]["img_src"]?>" alt="<?=$var["article"]["list"][0]["name"]?>" class="rounded event_img img-fluid" />
<?php
	}
?>
		</div>
	</div>

	<!--TEXT-->
	<div class="row my-4">
		<div class="col-12 article_text">
			<?=$var["article"]["list"][0]["text"]?>
		</div>
	</div>

	<!--FILES-->
<?php
	if(isset($var["article"]["list"][0]["files"]) AND $var["article"]["list"][0]["files"]!=''){
?>
	<section class="row border-top pb-4 pt-2">
		<div class="col-12">
		<h6>Файлы для скачивания</h6>
<?php
		foreach($var["article"]["list"][0]["files"] as $row){
?>
			<div class="mb-2 border-bottom">
				<?=$row["name"]!=''? '<strong>'.$row["name"].'</strong>' : ''?>
				<?=$row["desc"]!=''? '<p class="my-1">'.$row["desc"].'</p>' : ''?>
				<a class="btn btn-secondary btn-block text-left" href="/files/<?=$row["type"].'/'.$row["src"]?>" title="<?=$row["src"]?>"><i class="far fa-file-<?=$row["type"]?>"></i> <?=$row["src"]?></a>
			</div>
<?php
		}
?>
		</div>
	</section>
<?php
	}
?>

	<!--RATING-->
	<div class="row border-top">
		<div class="col-12">
<?php
	if(!User::id()){
?>
			<div class="form-group">
				<label>Вход через социальные сети</label>
				<div id="uLogin3dd6303d" data-ulogin="display=small;fields=first_name,last_name,email;optional=phone;sort=default;providers=vkontakte,odnoklassniki,mailru,facebook,yandex;redirect_uri=http%3A%2F%2F<?=$_SERVER["HTTP_HOST"]?>%2Fuser%2Fauth_ulogin%2F?src=<?=$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"]?>"></div>
			</div>

			<div class="form-group">
				<label>Войдите, чтобы оценить статью</label>
				<div id="rating_star">
					<div id="rating">
						<div id="rating_blank"></div>
						<div id="rating_votes"></div>
					</div>
					<div id="rating_info"></div>
				</div>
			</div>
<?php
	}else{
?>
			<div class="form-group">
				<label for="comment_create_rating"><?=User::name()?>, оцените статью</label>
				<div id="rating_star">
					<div id="rating">
						<div id="rating_blank"></div>
						<div id="rating_hover"></div>
						<div id="rating_votes"></div>
					</div>
					<div id="rating_info"></div>
				</div>
			</div>
<?php
	}
?>
		</div>
	</div>

	<!--RATING-->
	<footer class="row border-top">
		<div class="col-12 pb-4 pt-2">
			<h3 class="comments_title">Комментарии (<?=$var["article"]["list"][0]["comment_count"]?>)</h3>

<?php
	if(isset($var["comment"]["list"])){

		foreach($var["comment"]["list"] as $row){
?>
			<div class="comment_header">
				<div class="comment_date"><?=$row["create_nice"]?></div>
				<div class="comment_name"><?=$row["user_name"].' ['.$row["user_id"].']'?></div>
			</div>
			<div class="comment_body">
				<div class="comment_text text-justify"><?=$row["text"]!=''? $row["text"] : ''?></div>
			</div>
			<hr />
<?php
		}

	}
	if(User::id()>1){
?>
			<div id="comment_form">
				<div class="form-group">
					<label for="comment_create_text">Оставить комментарий (3-1000 символов)</label>
					<textarea class="form-control" id="comment_create_text" rows="3"></textarea>
				</div>
				<div class="form-group">
					<button id="btn_comment_create" disabled class="btn btn-secondary" onClick="comment_create()">Отправить</button>
				</div>
			</div>
		
<?php
	}else{
?>
			<span><strong>Войдите или зарегистрируйтесь, чтобы комментировать</strong></span>
<?php
	}
?>
		</div>
	</footer>


</article>

<?php
}
?>
<script type="text/javascript">
function comment_create(){
	wait_start();
	var params={
		"type": 1,
		"target_id": parseInt(<?=$var["article"]["list"][0]["id"]?>),
		"text": $('#comment_create_text').val()
	}

	var answer = jsonrpc_request("comment.create", params);
	if(typeof(answer["error"])=="undefined") {
		$('#comment_form').html('Спасибо! Ваш комментарий отправлен!');
	}else {
		$('#comment_form').html('Извините, произошла ошибка. <i class="text-muted">'+answer["error"]["message"]+'</i>');
	}
	wait_finish();
}
$(document).ready(function(){
	total_rating = <?=$var["article"]["list"][0]["rating"]!=''? $var["article"]["list"][0]["rating"] : 0?>; // итоговый ретинг
	var star_widht = Math.ceil(total_rating*16);

	$('#rating_votes').width(star_widht);
	$('#rating_info').append(total_rating);

<?php
	if(User::id()>1){
?>
	$("#comment_create_text").val('');
	$('#btn_comment_create').prop('disabled', true);

	var margin_doc = $("#rating").offset();

	$("#rating").mousemove(function(e){
		var widht_votes = e.pageX - margin_doc.left;
		user_score = Math.ceil(widht_votes/16); 
		$('#rating_hover').width(user_score*16);
		var hover_width=user_score*16;
		hover_width=hover_width+' px';
	});
<?php
	}
?>

});
<?php
	if(User::id()>1){
?>
$("#rating")
	.mouseover(function() {
		$('#rating_hover').css({
			"display":"block"
		});
	})
	.mouseout(function() {
		$('#rating_hover').css({
			"display":"none"
		});
	});

$("#comment_create_text")
	.keyup(function() {
		//console.log($('#comment_create_text').val().length);
		if($('#comment_create_text').val().length>2 && $('#comment_create_text').val().length<1001) $('#btn_comment_create').prop('disabled', false);
		else $('#btn_comment_create').prop('disabled', true);
	})


$('#rating').click(function(){
	wait_start();
	var params={
		"type": 1,
		"target_id": parseInt(<?=$var["article"]["list"][0]["id"]?>),
		"score": user_score
	}

	var answer = jsonrpc_request("rating.create", params);
	if(typeof(answer["error"])=="undefined") {
		var star_widht = Math.ceil(answer["result"]["rating"]*16);
		$('#rating_info').html(answer["result"]["rating"]+' Спасибо! Ваша оценка: '+user_score);
		$('#rating_votes').width(star_widht);
	}else {
		$('#rating_info').html('ошибка <i class="text-muted">'+answer["error"]["message"]+'</i>');
	}
	
	wait_finish();
	return true;
});
<?php
	}
?>

$(document).ready(function(){

			var owl = $('.hero_slider');

			owl.owlCarousel({
				items:1,
				loop:true,
				smartSpeed:800,
				autoplay:true,
				nav:false,
				dots:true
			});

});
</script>