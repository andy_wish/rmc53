<?php
//echo '<pre>';var_dump($var["article"]);
if($var["article"]["total"]>0){
?>



<div class="container wide">

	<!--TITLE-->
	<div class="row my-2">
		<div class="col-12">
			<!--<div class="my-1 event_date">
				<?=$var["article"]["list"][0]["created_nice"]?>&emsp;<?=$var["article"]["list"][0]["rating"]!=''? $var["article"]["list"][0]["rating"] : ''?>
			</div>-->
			<h1><?=$var["page"]["title"]?></h1>
		</div>
	</div>


	<!--TEXT-->
	<div class="row my-4">
		<div class="col-12 article_text">
			<?=$var["article"]["list"][0]["text"]?>
		</div>
	</div>

	<!--RATING-->
	<div class="row border-top">
		<div class="col-12 pb-4 pt-2">
<!--
			<div class="form-group">
				<script src="//ulogin.ru/js/ulogin.js"></script>
				<div id="uLogin" data-ulogin="display=small;theme=classic;fields=first_name,last_name;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=twitter,google,yandex,livejournal,googleplus,instagram;redirect_uri=http%3A%2F%2Frmc53%2Fuser%2Fauth_social%2F;mobilebuttons=0;"></div>
			</div>
-->
<?php
	if(!User::id()){
?>

			<div class="form-group">
				<label for="comment_create_rating">Войдите, чтобы оценить статью</label>
				<div id="rating_star">
					<div id="rating">
						<div id="rating_blank"></div>
						<div id="rating_votes"></div>
					</div>
					<div id="rating_info"></div>
				</div>
			</div>
<?php
	}else{
?>
			<div class="form-group">
				<label for="comment_create_rating"><?=User::name()?>, оцените статью</label>
				<div id="rating_star">
					<div id="rating">
						<div id="rating_blank"></div>
						<div id="rating_hover"></div>
						<div id="rating_votes"></div>
					</div>
					<div id="rating_info"></div>
				</div>
			</div>
<?php
	}
?>
		</div>
	</div>

	<!--RATING-->
	<div class="row border-top">
		<div class="col-12 pb-4 pt-2">
			<h3 class="comments_title">Комментарии (<?=$var["article"]["list"][0]["comment_count"]?>)</h3>

<?php
	if(isset($var["comment"]["list"])){

		foreach($var["comment"]["list"] as $row){
?>
			<div class="comment_header">
				<div class="comment_date"><?=$row["create_nice"]?></div>
				<div class="comment_name"><?=$row["user_name"].' ['.$row["user_id"].']'?></div>
			</div>
			<div class="comment_body">
				<div class="comment_text text-justify"><?=$row["text"]!=''? $row["text"] : ''?></div>
			</div>
			<hr />
<?php
		}

	}
	if(User::id()>1){
?>
			<div id="comment_form">
				<div class="form-group">
					<label for="comment_create_text">Оставить комментарий (3-1000 символов)</label>
					<textarea class="form-control" id="comment_create_text" rows="3"></textarea>
				</div>
				<div class="form-group">
					<button id="btn_comment_create" disabled class="btn btn-secondary" onClick="comment_create()">Отправить</button>
				</div>
			</div>
		
<?php
	}else{
?>
			<span><strong>Войдите или зарегистрируйтесь, чтобы комментировать</strong></span>
<?php
	}
?>
		</div>
	</div>


</div>

<?php
}
?>
<script type="text/javascript">
function comment_create(){
	wait_start();
	var params={
		"type": 2,
		"target_id": parseInt(<?=$var["article"]["list"][0]["id"]?>),
		"text": $('#comment_create_text').val()
	}

	var answer = jsonrpc_request("comment.create", params);
	if(typeof(answer["error"])=="undefined") {
		$('#comment_form').html('Спасибо! Ваш комментарий отправлен!');
	}else {
		$('#comment_form').html('Извините, произошла ошибка. <i class="text-muted">'+answer["error"]["message"]+'</i>');
	}
	wait_finish();
}
$(document).ready(function(){
	total_rating = <?=$var["article"]["list"][0]["rating"]!=''? $var["article"]["list"][0]["rating"] : 0?>; // итоговый ретинг
	var star_widht = Math.ceil(total_rating*17);

	$('#rating_votes').width(star_widht);
	$('#rating_info').append(total_rating);

<?php
	if(User::id()>1){
?>
	$("#comment_create_text").val('');
	$('#btn_comment_create').prop('disabled', true);

	var margin_doc = $("#rating").offset();

	$("#rating").mousemove(function(e){
		var widht_votes = e.pageX - margin_doc.left;
		user_score = Math.ceil(widht_votes/17); 
		$('#rating_hover').width(user_score*17);
		var hover_width=user_score*17;
		hover_width=hover_width+' px';
	});
<?php
	}
?>

});
<?php
	if(User::id()>1){
?>
$("#rating")
	.mouseover(function() {
		$('#rating_hover').css({
			"display":"block"
		});
	})
	.mouseout(function() {
		$('#rating_hover').css({
			"display":"none"
		});
	});

$("#comment_create_text")
	.keyup(function() {
		//console.log($('#comment_create_text').val().length);
		if($('#comment_create_text').val().length>2 && $('#comment_create_text').val().length<1001) $('#btn_comment_create').prop('disabled', false);
		else $('#btn_comment_create').prop('disabled', true);
	})


$('#rating').click(function(){
	wait_start();
	var params={
		"type": 2,
		"target_id": parseInt(<?=$var["article"]["list"][0]["id"]?>),
		"score": user_score
	}

	var answer = jsonrpc_request("rating.create", params);
	if(typeof(answer["error"])=="undefined") {
		var star_widht = Math.ceil(answer["result"]["rating"]*17);
		$('#rating_info').html(answer["result"]["rating"]+' Спасибо! Ваша оценка: '+user_score);
		$('#rating_votes').width(star_widht);
	}else {
		$('#rating_info').html('ошибка <i class="text-muted">'+answer["error"]["message"]+'</i>');
	}
	
	wait_finish();
	return true;
});
<?php
	}
?>
</script>