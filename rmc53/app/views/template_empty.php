<?php if (!defined("INBOX")) die('separate call');?>
<!doctype html><html lang="ru">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="umdi.ru">
	<title>LeadBox</title>

	<link rel="stylesheet" type="text/css" href="/plugins/bootstrap/bootstrap.min.css">
	<link href="/plugins/fontawesome/css/fontawesome-all.css" rel="stylesheet" type="text/css">

	<script src="/plugins/jquery-3.2.1.min.js"></script>
	<script src="/plugins/popper.min.js"></script>
	<script src="/plugins/bootstrap/bootstrap.min.js"></script>
</head>

<body>
<header>
</header>

<?php include 'admin/views/'.$content_file;?>

<footer class="footer">
	<div class="container text-center">
		<span class="text-muted"><a href="https://umdi.ru">umdi.ru</a></span>
	</div>
</footer>
</body>
</html>