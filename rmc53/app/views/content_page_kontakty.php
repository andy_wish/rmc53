<div class="container-fluid">
	<div class="row">
		<div style="width:100%;height:23rem;">
			<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9b205a612dff8cb1b9e95535d92c44140c7720acb6c584093fdafc105f64754a&amp;lang=ru_RU&amp;scroll=false"></script>
		</div>
	</div>
</div>

<div class="container article_read_wide">

	<!--TITLE-->
	<div class="row my-2">
		<div class="col-12">
			<h1><?=$var["page"]["title"]?></h1>
		</div>
	</div>


	<!--TEXT-->
	<div class="row my-4">
		<div class="col-12 article_text">
			<?=$var["article"]["list"][0]["text"]?>
		</div>
	</div>

</div>
