<?php if (!defined("INBOX")) die('separate call');

$conf["files"]=[

	"max_file_size"=> 52428800, //50Mb
	"types_allowed"=> [
		"banner"=>[
			"jpeg"=> 'image',
			"png"=> 'image',
			"gif"=> 'image'
		],
		"article"=>[
			"jpeg"=> 'image',
			"png"=> 'image',
			"gif"=> 'image',
			"pdf"=> 'pdf',
			"msword"=> 'word',
			"mspowerpoint"=> 'powerpoint',
			"vnd.ms-powerpoint"=> 'powerpoint',
			"x-zip-compressed"=> 'archive'
		],
		"poll"=>[
			"jpeg"=> 'image',
			"png"=> 'image',
			"gif"=> 'image',
			"pdf"=> 'pdf',
			"msword"=> 'word',
			"mspowerpoint"=> 'powerpoint',
			"vnd.ms-powerpoint"=> 'powerpoint',
			"x-zip-compressed"=> 'archive'
		]
	],
	"upload_accept"=>[
		"banner"=>'image/jpeg, image/png, image/gif',
		"article"=>'image/jpeg, image/png, image/gif, application/pdf, application/msword, application/zip, application/mspowerpoint, application/vnd.ms-powerpoint',
	]

];