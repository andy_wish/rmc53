<?php
$debug_mode=1;
if($debug_mode){
	ini_set('display_errors',1);
	error_reporting(E_ALL);
}

define('INBOX', 1);
define('ROOT_DIR', dirname(__FILE__));

spl_autoload_register(function($class) {
	require_once(ROOT_DIR.'/library/'.strtolower($class).'.php');
});


$routes=explode('/', $_SERVER['REQUEST_URI']);
switch($routes[1]){
	case 'admin':
		require ROOT_DIR.'/admin/boot.php';
		break;
	case 'jsonrpc':
		require ROOT_DIR.'/jsonrpc/boot.php';
		break;
	default:
		require ROOT_DIR.'/app/boot.php';
}

//if($debug_mode){echo ''.Auth::name().' ['.Auth::id().']<br />';echo ''.Auth::group_name().' ['.Auth::group_id().']<br />';}