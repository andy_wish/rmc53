<?php if (!defined("INBOX")) die('separate call');

class Model_Poll extends Model {

	public function index($var) {
		$var["page"]["title"]='Опросы';

		$var["poll"]=Poll::read_list([]);
		return $var;
	}

	public function listing($var) {//
		if(!isset($var[0])) Core::error_404();

		$params["response"]["type"]='one';
		$params["id"]=$var[0];
		$result=poll::read($params);
		
		$var["page"]["title"]=$result["name"];
		
		/////PARAMS
		$params=array();
		$params["cat_id"]=$result["id"];
			/////response
		$params["response"]["type"]='list';
		$params["response"]["text_preview"]=150;
		$params["response"]["status"]='all';
		//limit
		if(isset($var["get"]["limit"]) AND (int)$var["get"]["limit"]==$var["get"]["limit"] AND $var["get"]["limit"]>0 AND $var["get"]["limit"]<100) $params["response"]["limit"]=$var["get"]["limit"];
		else $params["response"]["limit"]=20;
		//offset
		if(isset($var["get"]["page"]) AND (int)$var["get"]["page"]==$var["get"]["page"] AND $var["get"]["page"]>0) {
			$var["get"]["page"]=$var["get"]["page"];
			$params["response"]["offset"]=($var["get"]["page"]-1)*$params["response"]["limit"];
			$var["pagination"]["current"]=(int)$var["get"]["page"];
		}else $var["pagination"]["current"]=1;
		//order
		if(isset($var["get"]["order"])) $params["response"]["order"]=$var["get"]["order"];
		else $params["response"]["order"]='create';
		//direction
		if(isset($var["get"]["dir"])) $params["response"]["direction"]=$var["get"]["dir"];
		else $params["response"]["direction"]='desc';
		
		$var["poll"]=poll::read($params);

		////PAGINATION
		if($var["poll"]["total_db"]>$params["response"]["limit"]){
			$var["pagination"]["finish"]=ceil($var["poll"]["total_db"]/$params["response"]["limit"]);
			if($var["pagination"]["finish"]<=$var["pagination"]["current"]){
				$var["pagination"]["finish"]=false;
				$var["pagination"]["next"]=false;
			}else{
				$var["pagination"]["next"]=$var["pagination"]["current"]+1;
			}

			if($var["pagination"]["current"]>1) {
				$var["pagination"]["start"]=1;
				$var["pagination"]["prev"]=$var["pagination"]["current"]-1;
			}else {
				$var["pagination"]["start"]=false;
				$var["pagination"]["prev"]=false;
			}

			if($var["pagination"]["prev"] AND ($var["pagination"]["prev"]-1)>0) $var["pagination"]["prev2"]=$var["pagination"]["prev"]-1;
			else $var["pagination"]["prev2"]=false;

			if($var["pagination"]["next"] AND ($var["pagination"]["next"]+1)<$var["pagination"]["finish"]) $var["pagination"]["next2"]=$var["pagination"]["next"]+1;
			else $var["pagination"]["next2"]=false;
			//echo '<pre>';var_dump($var["pagination"]);
		}else $var["pagination"]["current"]=false;


		return $var;
	}

	public function create($var) {//

		if(!isset($var[0])) Core::error_404();

		$var["id"]=false;
		$var["cat_id"]=(int)$var[0];

		$params["response"]["type"]='one';
		$params["id"]=$var["cat_id"];
		$result=poll::read($params);
		
		$var["cat_name"]=$result["name"];
		
		$var["form_action"]='/admin/poll/create/action/'.$var["cat_id"];
		$var["name"]='';
		$var["date"]='';
		$var["text"]='';
		$var["title"]='Создать';
		$var["show_files"]=false;

		return $var;
	}

	public function create_action($var) {//
		if(!isset($var["post"])) Core::error_404();
		if(isset($var[1])) $params["cat_id"]=(int)$var["1"];
		else {
			$var["error"]='cat_id required';
			return $var;
		}

		if(isset($var["post"]["name"])) $params["name"]=$var["post"]["name"];
		else {
			$var["error"]='name required';
			return $var;
		}

		if(isset($var["post"]["text"])) $params["text"]=$var["post"]["text"];

		$result=poll::create($params);

		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}else $var["success"]='Статья #'.$result["id"].' создана. Можете добавить файлы.';

		$params=array(
			"id"=>$result["id"],
			"response"=>array(
				"limit"=>1,
				"type"=>'read'
			)
		);
		$var["poll"]=poll::read($params);

		$var["id"]=$var["poll"]["list"][0]["id"];
		$var["cat_id"]=$var["poll"]["list"][0]["cat_id"];
		$var["cat_name"]=$var["poll"]["list"][0]["cat_name"];
		$var["form_action"]='/admin/poll/update/'.$var["cat_id"].'/'.$var["id"];
		$var["name"]=$var["poll"]["list"][0]["name"];
		$var["date"]=$var["poll"]["list"][0]["create_nice"];
		$var["text"]=$var["poll"]["list"][0]["text"];

		$var["title"]=$var["name"];
		$var["show_files"]=true;
		$var["files"]=$var["poll"]["list"][0]["files"];
//echo '<pre>';var_dump($var);
		return $var;
	}

	public function read_filled($var) {

		if(!isset($var[0])) Core::error_404();

		$params=[
			"id"=>(int)$var[0],
		];

		$var["poll"]=Poll::read_filled($params);

		return $var;
	}

	public function update($var) {//

		if(isset($var[1])) $params["id"]=(int)$var[1];
		else {
			$var["error"]='id required';
			return $var;
		}

		if(isset($var["post"]["name"])) $params["name"]=$var["post"]["name"];
		else {
			$var["error"]='name required';
			return $var;
		}

		if(isset($var["post"]["text"])) $params["text"]=$var["post"]["text"];

		$result=poll::update($params);

		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}

		$params=array(
			"id"=>$result["id"],
			"response"=>array(
				"limit"=>1,
				"type"=>'read'
			)
		);
		$var["poll"]=poll::read($params);

		$var["id"]=$var["poll"]["list"][0]["id"];
		$var["cat_id"]=$var["poll"]["list"][0]["cat_id"];
		$var["cat_name"]=$var["poll"]["list"][0]["cat_name"];
		$var["form_action"]='/admin/poll/update/'.$var["cat_id"].'/'.$var["id"];
		$var["name"]=$var["poll"]["list"][0]["name"];
		$var["date"]=$var["poll"]["list"][0]["create_nice"];
		$var["text"]=$var["poll"]["list"][0]["text"];

		$var["title"]=$var["name"];
		$var["show_files"]=true;
		if(isset($var["poll"]["list"][0]["files"]) AND $var["poll"]["list"][0]["files"]!='') {
			$var["files"]=$var["poll"]["list"][0]["files"];
		}else $var["files"]=array();

		return $var;
	}

	public function delete($var) {//

		if(!isset($var[0]) OR !isset($var[1])) Core::error_404();

		$params["id"]=(int)$var["1"];

		$result=poll::delete($params);
		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}else $var["success"]='Статья #'.$result["id"].' больше не показывается';

		$var=$this->listing($var);

		return $var;
	}

	public function restore($var) {//

		if(!isset($var[0]) OR !isset($var[1])) Core::error_404();

		$params["id"]=(int)$var["1"];
		$result=poll::restore($params);
		if(isset($result["error"])) {
			$var["error"]=$result["error"];
			return $var;
		}else $var["success"]='Статья #'.$result["id"].' показывается';

		$var=$this->listing($var);

		return $var;
	}

}
