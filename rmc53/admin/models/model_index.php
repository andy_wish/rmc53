<?php if (!defined("INBOX")) die('not allowed');

class Model_Index extends Model {

	public function index($var) {

		$params=[
			'response'=>[
				'order'=>'create',
				'direction'=>'desc',
				'limit'=>5,
				'offset'=>0
			],
		];
		$var["comment"]=Comment::read($params);

		return $var;
	}


}
