<?php if (!defined("INBOX")) die('separate call');

class Core {

	public static function get_header_data($var){

		return $var;
	}

	public static function get_footer_data($var){

		return $var;
	}

	static function error_404() {
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
		header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'404');
		exit;
	}
	/*
	static function error_data() {
		header('Location: /error_data.php');
	}
	*/
	public static function translit($s) {
		//$s = (string) $s; // преобразуем в строковое значение
		//$s = strip_tags($s); // убираем HTML-теги
		$s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
		$s = str_replace("+", " ", $s);
		$s = str_replace("/", " ", $s);
		$s = str_replace(".", " ", $s);
		$s = trim($s); // убираем пробелы в начале и конце строки
		$s = strtr($s, array(
			'а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>'',
			'А'=>'a','Б'=>'b','В'=>'v','Г'=>'g','Д'=>'d','Е'=>'e','Ё'=>'e','Ж'=>'j','З'=>'z','И'=>'i','Й'=>'y','К'=>'k','Л'=>'l','М'=>'m','Н'=>'n','О'=>'o','П'=>'p','Р'=>'r','С'=>'s','Т'=>'t','У'=>'u','Ф'=>'f','Х'=>'h','Ц'=>'c','Ч'=>'ch','Ш'=>'sh','Щ'=>'shch','Ы'=>'y','Э'=>'e','Ю'=>'yu','Я'=>'ya','Ъ'=>'','Ь'=>''
		));
		$s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
		$s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
		$s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
		return $s;
	}
}