<?php if (!defined("INBOX")) die('separate call');

class Controller_Index extends Controller {

	function __construct() {
		$this->model = new Model_Index();
		$this->view = new View();
	}

	function action_index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_index.php', $var);
	}

}
