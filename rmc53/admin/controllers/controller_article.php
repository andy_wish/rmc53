<?php if (!defined("INBOX")) die('separate call');

class Controller_Article extends Controller {

	function __construct() {

		$this->model=new Model_Article();
		$this->view=new View();
	}

	function action_index($var) {

		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_category_list.php', $var);
	}

	function action_create($var) {

		if(isset($var[0]) AND $var[0]=='action') $var=$this->model->create_action($var);
		else $var=$this->model->create($var);
		$this->view->generate('template.php', 'content_article.php', $var);
	}

	function action_read($var) {

		$var=$this->model->read($var);
		$this->view->generate('template.php', 'content_article.php', $var);
	}

	function action_update($var) {

		$var=$this->model->update($var);
		$this->view->generate('template.php', 'content_article.php', $var);
	}

	function action_delete($var) {

		$var=$this->model->delete($var);
		$this->view->generate('template.php', 'content_article_list.php', $var);
	}

	function action_restore($var) {

		$var=$this->model->restore($var);
		$this->view->generate('template.php', 'content_article_list.php', $var);
	}

	function action_list($var) {

		$var=$this->model->listing($var);
		$this->view->generate('template.php', 'content_article_list.php', $var);
	}

}
