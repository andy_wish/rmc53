<?php if (!defined("INBOX")) die('separate call');

class Controller_File extends Controller {

	function __construct() {
		$this->model=new Model_File();
		//$this->view=new View();
	}

	function action_create_ajax($var) {
		$this->model->create_ajax($var);
	}

	function action_update_ajax($var) {
		$this->model->update_ajax($var);
	}

	function action_delete_ajax($var) {
		$this->model->delete_ajax($var);
	}

}
