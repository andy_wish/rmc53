<?php if (!defined("INBOX")) die('separate call');

class Controller_Comment extends Controller {

	function __construct() {
		$this->model = new Model_Comment();
		$this->view = new View();
	}

	function action_index($var) {
		Core::error_404();
	}

	function action_delete($var) {
		$var=$this->model->delete($var);
		header('Location: /admin/');
	}
}
