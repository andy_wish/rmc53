<?php if (!defined("INBOX")) die('separate call');

class Controller_Poll extends Controller {

	function __construct() {
		$this->model=new Model_Poll();
		$this->view=new View();
	}

	function action_index($var) {
		$var=$this->model->index($var);
		$this->view->generate('template.php', 'content_poll_list.php', $var);
	}

	function action_create($var) {
		$var=$this->model->create($var);
		$this->view->generate('template.php', 'content_poll.php', $var);
	}

	function action_read($var) {
		$var=$this->model->read($var);
		$this->view->generate('template.php', 'content_poll.php', $var);
	}

	function action_read_filled($var) {
		$var=$this->model->read_filled($var);
		$this->view->generate('template.php', 'content_poll_filled.php', $var);
	}

	function action_update($var) {
		$var=$this->model->update($var);
		$this->view->generate('template.php', 'content_poll.php', $var);
	}

	function action_delete($var) {
		$var=$this->model->delete($var);
		$this->view->generate('template.php', 'content_poll_list.php', $var);
	}

}
