<?php if (!defined("INBOX")) die('separate call');

class Controller_Unauthorized extends Controller {

	function __construct(){
		$this->view = new View();
	}

	function action_index($var) {//
		$var["page"]["title"]='Войдите в систему';
		$this->view->generate('template_empty.php', 'content_unauthorized.php', $var);
	}

}
