<div class="container-fluid">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/admin/">Главная</a></li>
				<li class="breadcrumb-item"><a href="/admin/poll/">Опросы</a></li>
				<li class="breadcrumb-item active"><?=$var["poll"]["details"]["name"]?></li>
			</ol>
		</div>
	</div>
</div>

<div class="container-fluid">

<?php
if($var["poll"]["total"]>0){
/*
  ["fields"]=>
  array(6) {
    ["id"]=>
    string(1) "6"
    ["poll_id"]=>
    string(1) "2"
    ["order_num"]=>
    string(1) "1"
    ["name"]=>
    string(55) "ФИО обучающегося, организация"
    ["description"]=>
    NULL
    ["type_id"]=>
    string(1) "5"
  }
  ["list"]=>
  array(13) {
    [0]=>
    array(9) {
      ["id"]=>
      string(2) "62"
      ["poll_id"]=>
      string(1) "2"
      ["field_id"]=>
      string(1) "6"
      ["user_tag"]=>
      string(10) "921c8bc409"
      ["user_id"]=>
      string(1) "0"
      ["filler"]=>
      string(58) "Вишневская Полина Владимировна"
      ["ip"]=>
      string(22) "81.9.27.65, 81.9.27.65"
      ["created"]=>
      string(19) "2018-09-20 15:28:12"
      ["created_nice"]=>
      string(14) "20.09.18 15:28"
    }
*/
?>
<div class="row justify-content-center">
	<div class="col-xl-11 col-lg-12">
		<table class="table table-sm table-striped table-bordered" id="polls">
			<thead>
				<tr>
					
<?php
	$fields_count=0;
	foreach($var["poll"]["fields"] as $field){
		$fields_count++;
?>
					<th><?=$field["order_num"].'. '.$field["name"]?></th>
<?php
	}
?>
				</tr>
			</thead>
			<tbody>
		<tr>
<?php
	$field_current=0;
	foreach($var["poll"]["list"] as $row){
		$field_current++;
?>

			<td><?=$row["filler"]?></td>
<?php
		if($field_current==$fields_count){
			$field_current=0;
?>
				</tr><tr>
<?php
		}
		
	}
?>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<?php
}else{
?>
<div class="row justify-content-center">
	<div class="сol-12 my-2"><p>не найдено</p></div>
</div>
<?php
}
?>

</div>

<script type="text/javascript">

function poll_update(id, target, val){
	//var val=$('#poll_'+id+'_'+target).val();
	var params={"id": parseInt(id)};

	switch(target){
		case 'name':
			params["name"]=val;
			break;
		case 'description':
			params["description"]=val;
			break;
		case 'status':
			if($('#status_btn_'+id).hasClass('btn-success')){
				$('#status_btn_'+id).removeClass('btn-success').addClass('btn-danger');
				params["status"]=0;
			}else{
				$('#status_btn_'+id).removeClass('btn-danger').addClass('btn-success');
				params["status"]=1 ;
			}
			break;
		default:
			return;
	}
	jsonrpc_request("poll.update", params, 1, true);

}
</script>