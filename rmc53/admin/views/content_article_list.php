<?php $cat_id=$var[0];?>
<div class="container px-md-5 px-sm-3 px-xs-0">

	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/admin/">Главная</a></li>
				<li class="breadcrumb-item"><a href="/admin/article">Статьи</a></li>
				<li class="breadcrumb-item active"><?=$var["page"]["title"]?></li>
			</ol>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-12">
			<h1>
			<a href="/admin/article/create/<?=$cat_id?>" class="btn btn-secondary">
				<i class="far fa-plus-square"></i>
			</a>
			<?=$var["page"]["title"]?>
		</h1>
		</div>
	</div>

<?php
if($var["article"]["total"]>0){
?>
<div class="row justify-content-center">
	<div class="col-xl-8 col-lg-9 col-md-12">
		<table class="table table-sm table-striped table-bordered">
			<thead>
				<tr>
					<th>№</th>
					<th></th>
					<th>дата</th>
					<th>Название</th>
					<th></th>
					<th>Файлы</th>
					<th>статус</th>
				</tr>
			</thead>
<?php
	foreach($var["article"]["list"] as $row){
		if($row["img_src"]!='')$img_src='/files/image/'.$row["img_src"];
		else $img_src='/files/image/no-image.jpg';
		
		if($row["close"]=='') {
			$active_mark='';
			$status_btn='<a href="/admin/article/delete/'.$cat_id.'/'.$row["id"].'" id="status_btn_'.$row["id"].'" class="btn btn-sm btn-success py-0 px-1" title="отключить"><i class="fas fa-toggle-on"></i></a>';
		}else{
			$active_mark=' text-muted';
			$status_btn='<a href="/admin/article/restore/'.$cat_id.'/'.$row["id"].'" id="status_btn_'.$row["id"].'" class="btn btn-sm btn-danger py-0 px-1" title="включить"><i class="fas fa-toggle-off"></i></a>';
		}
?>
			<tr class="<?=$active_mark?>">
				<td>
					<?=$row["id"]?>
				</td>
				<td>
					<a href="/admin/article/read/<?=$row["cat_id"]?>/<?=$row["id"]?>/"><img src="<?=$img_src?>" alt="<?=$row["name"]?>" class="rounded event_img" style="width:5rem"/></a>
				</td>
				<td>
					<?=$row["create_nice"]?>
				</td>
				<td>
					<a href="/admin/article/read/<?=$row["cat_id"]?>/<?=$row["id"]?>/"><?=$row["name"]?></a>
				</td>
				<td>
					<p><?=$row["text"]?></p>
				</td>
				<td>
					<?=$row["file_count"]?>
				</td>
				<td>
					<?=$status_btn?>
				</td>
			</tr>
<?php
	}
?>
		</table>
	</div>
</div>

<?php
	if($var["pagination"]["current"]){
?>
<div class="row my-3 justify-content-center">
	<div class="сol-12" id="pagination">
		<div class="btn-toolbar" role="toolbar" aria-label="Pagination">
<?php

		if($var["pagination"]["start"]) {
?>
			<div class="btn-group mr-2" role="group" aria-label="В начало страниц">
				<a href="<?=$var["page"]["url"]?>" class="btn btn-secondary">В начало</a>
			</div>
<?php
		}
?>
			<div class="btn-group mr-2" role="group" aria-label="">
<?php
		if($var["pagination"]["prev"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev"]?>" class="btn btn-secondary"><i class="fas fa-arrow-left"></i></a>
<?php
		}
		if($var["pagination"]["prev2"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev2"]?>" class="btn btn-secondary"><?=$var["pagination"]["prev2"]?></a>
<?php
		}
		if($var["pagination"]["prev"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["prev"]?>" class="btn btn-secondary"><?=$var["pagination"]["prev"]?></a>
<?php
		}
?>

				<span class="btn btn-warning pagination_current"><?=$var["pagination"]["current"]?></span>

<?php
		if($var["pagination"]["next"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next"]?>" class="btn btn-secondary"><?=$var["pagination"]["next"]?></a>
<?php
		}
		if($var["pagination"]["next2"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next2"]?>" class="btn btn-secondary"><?=$var["pagination"]["next2"]?></a>
<?php
		}
		if($var["pagination"]["next"]) {
?>
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["next"]?>" class="btn btn-secondary"><i class="fas fa-arrow-right"></i></a>
<?php
		}
?>
			</div>
<?php
		if($var["pagination"]["finish"]) {
?>
			<div class="btn-group mr-2" role="group" aria-label="В конец страниц">
				<a href="<?=$var["page"]["url"].'?page='.$var["pagination"]["finish"]?>" class="btn btn-secondary">В конец</a>
			</div>
<?php
		}
?>
		</div>
	</div>
</div>
	
<?php
	}
}else{
?>
<div class="row justify-content-center">
	<div class="сol-12 my-2"><p>Статьи не найдены</p></div>
</div>
<?php
}
?>

</div>