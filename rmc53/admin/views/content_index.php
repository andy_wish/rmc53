<main role="main" class="container">
	<h1 class="m-2">Административная панель</h1>
	<hr />
	<h6>Последние комментарии</h6>
<?php
/*
        ["type"]
        ["target_id"]
        ["ip"]
*/

if(isset($var["comment"]) AND $var["comment"]["total"]>0){
	foreach($var["comment"]["list"] AS $comment){
?>
	<div class="comment_header">
		<a class="btn btn-secondary btn-sm py-0" href="/admin/comment/delete/<?=$comment["id"]?>" title="удалить"><small><i class="far fa-times-circle"></i></small></a>
		<small class="text-muted" title="<?=$comment["create"]?>"><?=$comment["create_nice"]?></small>
		<small><strong><?=$comment["user_name"].' ['.$comment["user_id"].']'?></strong></small>
		<small class="text-muted" title="возможный IP"><?=$comment["ip"]?></small>
	</div>
	<div class="comment_body alert alert-secondary mb-4">
		<?=$comment["text"]!=''? $comment["text"] : ''?>
	</div>
<?php
	}
}
?>

</main>