<div class="container px-md-5 px-sm-3 px-xs-0">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/admin/">Главная</a></li>
				<li class="breadcrumb-item"><a href="/admin/article">Статьи</a></li>
				<li class="breadcrumb-item"><a href="/admin/article/list/<?=$var["cat_id"]?>"><?=$var["cat_name"]?></a></li>
				<li class="breadcrumb-item active"><?=$var["title"]?></li>
			</ol>
		</div>
	</div>


<div class="row">

	<div class="col-lg-10 col-md-12 mb-3">
		<form class="col-12" action="<?=$var["form_action"]?>" method="post">
			<div class="form-group">
				<label><?=$var["date"]?></label>
				<input type="text" class="form-control" placeholder="название, обязательно" name="name" value="<?=$var["name"]?>" />
			</div>
			<div class="form-group">
				<textarea class="form-control" rows="10" id="editor1" name="text" placeholder="текст, не обязательно"><?=$var["text"]?></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-success" type="submit">Сохранить</button>
			</div>
		</form>
	</div>


	<div class="col-lg-10 col-md-12">
		<h3>Файлы</h3>
<?php
if($var["show_files"]){
?>
		<div class="form-group">
			<label for="article_files_input">Добавить файлы</label>
			<input type="file" onChange="article_file_create(this.files)" multiple="multiple" accept="<?=Files::upload_accept_type('article')?>" class="form-control-file" id="article_files_input" />
		</div>
		<hr />

		<table id="article_files" class="table table-sm table-striped table-bordered">
			<thead>
				<tr>
					<th></th>
					<th>Превью</th>
					<th>на диске</th>
					<th>Название</th>
					<th>Описание</th>
					<th></th>
				</tr>
			</thead>
<?php
	if(isset($var["images"]) AND $var["images"]!=''){
		foreach($var["images"] as $row){
?>
				<tr id="article_file_<?=$row["id"]?>">
					<td>
						<input class="form-control" type="number" min="1" max="100" value="<?=$row["order"]?>" id="article_file_<?=$row["id"]?>_order" style="width:4.5rem" onChange="article_file_update(<?=$row["id"]?>, 'order')" />
					</td>
					<td><a href="/files/<?=$row["type"]?>/<?=$row["src"]?>"><img src="/files/<?=$row["type"]?>/<?=$row["src"]?>" width="100" class="rounded" /></a></td>
					<td><span><?=$row["src"]?></span><br /><span class="text-muted"><?=$row["type"]?></span></td>
					<td title="<?=$row["id"]?>">
						<input class="form-control" type="text" value="<?=isset($row["name"])? $row["name"]: ''?>" id="article_file_<?=$row["id"]?>_name" onKeyUp="article_file_update(<?=$row["id"]?>, 'name')" />
					</td>
					<td>
						<input class="form-control" type="text" value="<?=isset($row["desc"])? $row["desc"] : ''?>" id="article_file_<?=$row["id"]?>_desc" onKeyUp="article_file_update(<?=$row["id"]?>, 'desc')" />
					</td>
					<td title="Удалить"><button class="btn btn-sm btn-danger py-0 px-1" onClick="article_file_delete(<?=$row["id"]?>, '<?=$row["src"]?>', '<?=$row["type"]?>')"><i class="fas fa-trash-alt"></i></button></td>
				</tr>
<?php
		}
	}
	if(isset($var["files"]) AND $var["files"]!=''){

		foreach($var["files"] as $row){
			switch($row["type"]){
				case 'image':
					$preview='<a href="/files/'.$row["type"].'/'.$row["src"].'"><img src="/files/'.$row["type"].'/'.$row["src"].'" width="100" class="rounded" /></a>';

			}
?>
				<tr id="article_file_<?=$row["id"]?>">
					<td>
						<input class="form-control" type="number" min="1" max="100" value="<?=$row["order"]?>" id="article_file_<?=$row["id"]?>_order" style="width:4.5rem" onChange="article_file_update(<?=$row["id"]?>, 'order')" />
					</td>
					<td><a class="btn btn-secondary btn-lg" href="/files/<?=$row["type"]?>/<?=$row["src"]?>"><i class="far fa-file-<?=$row["type"]?>"></i></a></td>
					<td><span><?=$row["src"]?></span><br /><span class="text-muted"><?=$row["type"]?></span></td>
					<td title="<?=$row["id"]?>">
						<input class="form-control" type="text" value="<?=isset($row["name"])? $row["name"]: ''?>" id="article_file_<?=$row["id"]?>_name" onKeyUp="article_file_update(<?=$row["id"]?>, 'name')" />
					</td>
					<td>
						<input class="form-control" type="text" value="<?=isset($row["desc"])? $row["desc"] : ''?>" id="article_file_<?=$row["id"]?>_desc" onKeyUp="article_file_update(<?=$row["id"]?>, 'desc')" />
					</td>
					<td title="Удалить"><button class="btn btn-sm btn-danger py-0 px-1" onClick="article_file_delete(<?=$row["id"]?>, '<?=$row["src"]?>', '<?=$row["type"]?>')"><i class="fas fa-trash-alt"></i></button></td>
				</tr>
<?php
		}
	}
?>
		</table>

		<script type="text/javascript">
var article_id=<?=$var["id"]?>;

function article_file_update(id, target){

	var val=$('#article_file_'+id+'_'+target).val();

	var data = new FormData();
	data.append('id', parseInt(id));
	data.append(target, val);

	$.ajax({
		url         : '/admin/file/update_ajax/',
		type        : 'POST',
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса это строковой запрос
		contentType : false, 
		success     : function( respond, status, jqXHR ){
		},
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});
}

function article_file_delete(id, src, type){
	if(confirm("Удалить?")){

		var data = new FormData();
		data.append('id', parseInt(id));
		data.append('type', type);
		data.append('src', src);
		$.ajax({
			url         : '/admin/file/delete_ajax/',
			type        : 'POST',
			data        : data,
			cache       : false,
			dataType    : 'json',
			processData : false,
			contentType : false, 
			success     : function(respond, status, jqXHR ){
				if(typeof respond.error === 'undefined' ){
					$('#article_file_'+respond.id).remove();
				}else console.log('AJAX ERROR: '+respond.error);
			},
			error: function(jqXHR, status, errorThrown){
				console.log('AJAX REQUEST ERROR: '+status, jqXHR);
			}

		});
	}
}

function article_file_create(files){

	if(typeof files=='undefined') return;

	var data=new FormData();
	$.each(files, function(key, value){
		data.append(key, value);
	});
	data.append('article_id', article_id);

	$.ajax({
		url			: '/admin/file/create_ajax',
		type		: 'POST',
		data		: data,
		cache		: false,
		dataType	: 'json',
		processData	: false,
		contentType	: false,
		success     : function(respond, status, jqXHR ){
			if(typeof respond.error === 'undefined' ){
				//$('#article_file_'+respond.id).remove();
				$.each(respond.list, function(key, value){
					var preview='';

					if(this["type"]=='image') preview='<a href="/files/'+this["type"]+'/'+this["src"]+'"><img src="/files/'+this["type"]+'/'+this["src"]+'" width="100" class="rounded" /></a>';
					else preview='<a class="btn btn-secondary btn-lg" href="/files/'+this["type"]+'/'+this["src"]+'"><i class="far fa-file-'+this["type"]+'"></i></a>';

					$('#article_files').append(
						$('<tr id="article_file_'+this["id"]+'">').append(
							$('<td>').append(
								$('<input class="form-control" type="number" min="1" max="100" value="5" id="article_file_'+this["id"]+'_order" style="width:4.5rem" onChange="article_file_update('+this["id"]+', \'order\')" />')
							),
							$('<td title="id: '+this["id"]+'">').append(
								$(preview)
							),
							$('<td title="'+this["id"]+'">').append(
								$('<span>'+this["src"]+'</span><br /><span class="text-muted">'+this["type"]+',</span>&nbsp;<span class="text-muted">'+this["size_nice"]+'</span>')
							),
							$('<td>').append(
								$('<input class="form-control" type="text" value="" id="article_file_'+this["id"]+'_name" onKeyUp="article_file_update('+this["id"]+', \'name\')" />')
							),
							$('<td title="'+this["id"]+'">').append(
								$('<input class="form-control" type="text" value="" id="article_file_'+this["id"]+'_desc" onKeyUp="article_file_update('+this["id"]+', \'desc\')" />')
							),
							$('<td title="Удалить">').append(
								$('<button class="btn btn-sm btn-danger py-0 px-1" onClick="article_file_delete('+this["id"]+', \''+this["src"]+'\', \''+this["type"]+'\')"><i class="fas fa-trash-alt"></i></button>')
							),
						)
					);
				});
			}else console.log('AJAX ERROR: '+respond.error);
		},
		error: function(jqXHR, status, errorThrown){
			console.log('AJAX REQUEST ERROR: '+status, jqXHR);
		}
	});

};



</script>
<?php
}else{
?>
	<p>Для добавления файлов, сохраните статью</p>
<?php
}
?>
	</div>

</div>






</div>
<script>
	CKEDITOR.replace( 'editor1' );
</script>