<div class="container px-md-5 px-sm-3 px-xs-0">
	<div class="row">
		<div class="col-12 px-0">
			<ol class="breadcrumb py-1">
				<li class="breadcrumb-item"><a href="/admin/">Главная</a></li>
				<li class="breadcrumb-item active"><?=$var["page"]["title"]?></li>
			</ol>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-12">
			<h1><?=$var["page"]["title"]?></h1>
		</div>
	</div>


<div class="row justify-content-center">
	<div class="col-xl-10 col-lg-11 col-md-12">
	<table class="table table-sm table-striped table-bordered">
<?php
foreach($var["category"] as $row){
?>
		<tr class="table-success">
			<th class="p-2"scope="row"><?=$row["name"]?></th>
		</tr>
<?php
	if($row["sub"]!=''){
		foreach($row["sub"] as $sub_row){
?>
		<tr>
			<td class="pl-4">
				<a class="button btn-sm btn-secondary" href="/admin/article/list/<?=$sub_row["sub_id"]?>"><?=$sub_row["sub_name"]?></a>
				<span class="badge badge-secondary"><?=$sub_row["sub_article_count"]?></span>
			</td>
		</tr>
<?php
		}
?>
					</div>
<?php
	}
}
?>
	</table>
	</div>
</div>

</div>