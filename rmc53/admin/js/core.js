
function modal_clear(position){
	$('#'+position+'_modal_header').empty();
	$('#'+position+'_modal_body').empty();
	$('#'+position+'_modal_footer').empty();
}
function modal_show(position, width='60rem'){
	$('.modal_box').css('max-width', width);
	$('#'+position+'_modal').modal();
}
function modal_close(position){
	$('#'+position+'_modal').modal('hide');
}

function wait_start(){
	$('#wait_notify').fadeIn();
}
function wait_finish(result=1, desc=''){
	$('#wait_notify').fadeOut();
}

function setCookie(n,v,h) {var d = new Date();d.setTime(d.getTime() + (h*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=n+"="+v+";"+expires+";path=/";}
function getCookie(n) {var n=n+"=";var decodedCookie=decodeURIComponent(document.cookie);var ca=decodedCookie.split(';');for(var i=0;i<ca.length;i++) {var c=ca[i];while (c.charAt(0)==' '){c=c.substring(1);}if (c.indexOf(n)==0) {return c.substring(n.length,c.length);}}return "";}