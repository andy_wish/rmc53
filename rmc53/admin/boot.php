<?php if (!defined("INBOX")) die('separate call');

require ROOT_DIR.'/admin/core/core.php';
require ROOT_DIR.'/admin/core/view.php';
require ROOT_DIR.'/admin/core/controller.php';
require ROOT_DIR.'/admin/core/model.php';
require ROOT_DIR.'/library/db.php';
require ROOT_DIR.'/library/verify.php';
require ROOT_DIR.'/library/notify.php';
require ROOT_DIR.'/library/files.php';
require ROOT_DIR.'/library/banner.php';


require ROOT_DIR.'/library/user.php';
if(isset($_COOKIE["data"])) {
	User::auth($_COOKIE["data"]);
	unset($_COOKIE["data"]);
}else User::auth(false);

require ROOT_DIR.'/admin/access.php';
require ROOT_DIR.'/library/access.php';
Access::init($conf["access"]);

require ROOT_DIR.'/admin/core/route.php';
Route::start();