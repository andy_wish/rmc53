<?php if (!defined("INBOX")) die('not allowed');

class Controller_Article extends Controller {

	function __construct(){
		$this->model = new Model_Article();
	}

	function create($params) {
		Response::result($this->model->create($params));
	}

	function read($params) {
		Response::result($this->model->read($params));
	}

	function delete($params) {
		Response::result($this->model->delete($params));
	}

}
