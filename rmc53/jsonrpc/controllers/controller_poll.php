<?php if (!defined("INBOX")) die('separate call');

class Controller_Poll extends Controller {

	function __construct(){
		$this->model = new Model_Poll();
	}

	function create($params) {
		Response::result($this->model->create($params));
	}

	function read($params) {
		Response::result($this->model->read($params));
	}
	
	function update($params) {
		Response::result($this->model->update($params));
	}
	
	function delete($params) {
		Response::result($this->model->delete($params));
	}

	function fill($params) {
		Response::result($this->model->fill($params));
	}

}
