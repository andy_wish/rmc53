<?php if (!defined("INBOX")) die('not allowed');

class Controller_Banner extends Controller {

	function __construct(){
		$this->model = new Model_Banner();
	}

	function create($params) {
		Response::result($this->model->create($params));
	}

	function read($params) {
		Response::result($this->model->read($params));
	}

	function update($params) {
		Response::result($this->model->update($params));
	}

	function delete($params) {
		Response::result($this->model->delete($params));
	}

}
