<?php if (!defined("INBOX")) die('not allowed');

class Controller_Comment extends Controller {

	function __construct(){
		$this->model = new Model_Comment();
	}

	function create($params) {
		Response::result($this->model->create($params));
	}
	function create_feedback($params) {
		Response::result($this->model->create_feedback($params));
	}
	
	function read($params) {
		Response::result($this->model->read($params));
	}

	function delete($params) {
		Response::result($this->model->delete($params));
	}

}
