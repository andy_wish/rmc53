<?php if (!defined("INBOX")) die('separate call');

class Model_Banner  extends Model {

	public function create($params) {
		return banner::create($params);
	}

	public function read($params) {
		return banner::read($params);
	}

	public function update($params) {
		return banner::update($params);
	}

	public function delete($params) {
		return banner::delete($params);
	}

}