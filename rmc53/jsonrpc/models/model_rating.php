<?php if (!defined("INBOX")) die('separate call');

class Model_Rating  extends Model {

	public function create($params) {
		return rating::create($params);
	}

	public function read($params) {
		return rating::read($params);
	}

	public function delete($params) {
		return rating::delete($params);
	}

}