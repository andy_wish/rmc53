<?php if (!defined("INBOX")) die('separate call');

class Model_Poll extends Model {

	public function create($params) {
		return poll::create($params);
	}

	public function read($params) {
		return poll::read($params);
	}

	public function update($params) {
		return poll::update($params);
	}

	public function delete($params) {
		return poll::delete($params);
	}

	public function fill($params) {
		return poll::fill($params);
	}

}