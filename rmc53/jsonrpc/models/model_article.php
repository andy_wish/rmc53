<?php if (!defined("INBOX")) die('separate call');

class Model_Article  extends Model {

	public function create($params) {
		return article::create($params);
	}
	
	public function read($params) {
		return article::read($params);

	}

	public function delete($params) {
		return article::delete($params);
	}

}