<?php if (!defined("INBOX")) die('separate call');

class Model_Comment  extends Model {

	public function create($params) {
		return Comment::create($params);
	}

	public function create_feedback($params) {

		if(!isset($params["name"])) return ["id"=>0];
		if(!isset($params["email"])) return ["id"=>0];
		if(!isset($params["text"])) return ["id"=>0];
		if(!isset($params["user_r"])) return ["id"=>0];
		if($params["user_r"]!='valid_user_true') return ["id"=>0];

		$comment_params=[
			"type"=>3,//feedback
			"target_id"=>0,
			"text"=>'Сообщение от '.$params["name"].'. email: '.$params["email"].'.'
		];
		if(isset($params["phone"]) AND $params["phone"]!='') $comment_params["text"].=' phone: '.$params["phone"].'.';
		$comment_params["text"].=' | '.$params["text"];

		$result=Comment::create($comment_params);
		if(!isset($result["error"])){
			Notify::feedback($params);
		}
		return $result;
	}

	public function read($params) {
		return Comment::read($params);
	}

	public function delete($params) {
		return Comment::delete($params);
	}

}