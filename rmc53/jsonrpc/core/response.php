<?php if (!defined("INBOX")) die('separate call');

class Response {

	const JSON_RPC_VERSION='2.0';

	protected static $_instance;

	private static $id=null;
	private static $result;

	private function __clone(){}
	private function __wakeup(){}
	public function __construct(){}

	public static function verify($request){
		if (self::$_instance === null) {
			self::$_instance = new self();
			
			if(isset($request["id"])) {
				if($request["id"]!=null) self::$id=$request["id"];
				else self::error('id is null');
			}

			if(!isset($request["jsonrpc"])) self::error('Invalid Request, jsonrpc not found', -32768);
			if($request["jsonrpc"]!==self::JSON_RPC_VERSION) self::error('Only jsonrpc 2.0 supported', -32600);
			if(!isset($request["method"])) self::error('Invalid Request, method not found', -32601);
			if(!is_string($request["method"])) self::error('Invalid Request, method must be a string', -32601);
			if(strlen($request["method"])<3 OR strlen($request["method"])>100) self::error('Invalid Request, method must be 3-100 characters', -32601);
			//if(!isset($request["params"]["access_token"]) AND $request["method"]!='auth.token_refresh' AND $request["method"]!='auth.login') self::error('access token required or methods auth.refresh_token/auth.login', 1);
		}
	}

	public static function error($m, $c=0, $d=null){
		$response["error"]["code"]=$c;
		$response["error"]["message"]=$m;
		if(isset($d)) $response["error"]["data"]=$d;
		self::send($response);
	}

	public static function result($result){
		$response["result"]=$result;
		self::send($response);
	}

	private static function send($response){
		$response["jsonrpc"]=self::JSON_RPC_VERSION;
		$response["id"]=self::$id;
		header("Access-Control-Allow-Origin: *");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400'); 
		echo json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
		exit;
	}

}
