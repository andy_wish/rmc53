<?php if (!defined("INBOX")) die('separate call');

class Route {

	static function start($request) {

		$routes = explode('.', $request["method"]);

		$controller=$routes[0];
		$action=$routes[1];

		if(!Access::permit($controller, $action)){
			Response::error('unauthorized access');
		}

		$model = 'Model_'.$controller;
		$controller = 'Controller_'.$controller;

		$controller_path = ROOT_DIR.'/jsonrpc/controllers/'.strtolower($controller).'.php';
		if(file_exists($controller_path)) {
			include $controller_path;

			$model_path = ROOT_DIR.'/jsonrpc/models/'.strtolower($model).'.php';
			if(file_exists($model_path)) {
				include $model_path;
			}

		}else Response::error('controller`s file not found: '.$controller);

		$controller = new $controller;

		if(method_exists($controller, $action)) {
			$controller->$action($request["params"]);
		}else Response::error('action`s method not found: '.$action);
	}

}