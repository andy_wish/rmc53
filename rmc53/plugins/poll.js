"0.1Alpha";
//var poll;
var poll_data

document.addEventListener("DOMContentLoaded", poll_init);

function poll_init(){

	var list=document.querySelectorAll('[data-poll]');
	if(list.length==0) return true;
	//poll={}

	poll_data=pollGetCookie('poll_data');
	if(!poll_data) {
		poll_data=generateId(10);
		pollSetCookie('poll_data', poll_data, 43800)
	}

	list.forEach(function(e) {
		poll_draw(e);
	});

}

function poll_draw(base_el){

	var poll_id=base_el.getAttribute('data-poll')
	var answer = poll_request("poll.read", {"id": poll_id});
	if(typeof(answer["result"]["error"])!="undefined") {
		base_el.innerHTML=answer["result"]["error"];
		return true;
	}
	//poll=answer["result"];
	//console.log(poll);
	var poll_name='poll_'+poll_id;
	base_el.innerHTML=answer["result"]["html"];
	var poll=document.getElementById('poll_'+poll_id)
	poll.t.value=poll_data
	poll.s.value=Math.round(new Date().getTime()/1000.0)

}

function poll_request(method, params, id=1, async=false, callback=false){

	//if(token=getCookie('data')) params["token"]=token;

	var data={"jsonrpc":"2.0", "method":method, "params":params, "id":id};
	var req;
	try {
		req=new ActiveXObject("Msxml2.XMLHTTP");
	}catch(e){
		try{
			req=new ActiveXObject("Microsoft.XMLHTTP");
		}catch(E){
			req=false;
		}
	}
	if(!req&&typeof XMLHttpRequest!='undefined') req=new XMLHttpRequest();

	req.open('post', 'http://rmc53.ru/jsonrpc/', async);
	
	req.send(JSON.stringify(data));
	if(!async){
		if(req.status==200) return JSON.parse(req.responseText);
	}else if(callback){
		req.onreadystatechange = function () {
			if(req.readyState==4 && req.status==200) return callback(JSON.parse(req.responseText));
		}
	}

}

function pollSetCookie(n,v,h) {var d = new Date();d.setTime(d.getTime() + (h*60*60*1000));var expires="expires="+ d.toUTCString();document.cookie=n+"="+v+";"+expires+";path=/";}
function pollGetCookie(n) {var n=n+"=";var decodedCookie=decodeURIComponent(document.cookie);var ca=decodedCookie.split(';');for(var i=0;i<ca.length;i++) {var c=ca[i];while (c.charAt(0)==' '){c=c.substring(1);}if (c.indexOf(n)==0) {return c.substring(n.length,c.length);}}return "";}

// random string
function dec2hex (dec) {return ('0' + dec.toString(16)).substr(-2);}
function generateId (len) {var arr = new Uint8Array((len || 40) / 2);window.crypto.getRandomValues(arr);return Array.from(arr, dec2hex).join('');}