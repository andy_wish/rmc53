<?php if (!defined("INBOX")) die('separate call');

class Rating {

	public static function verify($params) {

		foreach ($params as $name=>$value){
			switch ($name){
				case "id":
				case "type":
				case "target_id":
					if(!is_integer($value) OR $value<0 OR $value>9999999999) return array('error'=>$name.' must be positive integer. ['.$value.']');
					break;
				case "score":
					if(!is_integer($value) OR ($value!=1 AND $value!=2 AND $value!=3 AND $value!=4 AND $value!=5)) return array('error'=>$name.' must be positive integer 1-5. ['.$value.']');
					break;
				default:
					 return array('error'=>'unknown parameter ['.$name.'] = ['.$value.']');
					break;
			}
		}
		return $params;
	}

	public static function create($params) {

		$desc=[
			"target_id"=>[
				"type"=>'int',
				"min"=>0,
				"require"=>true
			],
			"type"=>[
				"type"=>'int',
				"require"=>true
			],
			"score"=>[
				"type"=>'int',
				"min"=>1,
				"max"=>5,
				"require"=>true
			]
		];

		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		$q='
			INSERT INTO `rating` (`user_id`, `type`, `target_id`, `ip`, `score`)
			VALUES (?i, ?i, ?i, ?s, ?i)
			ON DUPLICATE KEY UPDATE `ip`=?s, `score`=?i, `create`=NOW()
		';
		DB::query($q, User::id(), $params["type"], $params["target_id"], User::ip(), $params["score"], User::ip(), $params["score"]);
		$id=DB::insertId();
		if($id) {
			$rating=DB::getOne('SELECT FORMAT(AVG(`score`), 1) FROM `rating` WHERE `type`=?i AND `target_id`=?i', $params["type"], $params["target_id"]);
			return array("id"=>$id, "rating"=>$rating);
		}else return array('error'=>'db error');

		return $result;
	}

	public static function read($params) {

		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`rating`.`id`';
					break;
				case 'create':
					$order_by='`rating`.`create`';
					break;
				default:
					$order_by='`rating`.`create`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='asc') $order_dir='ASC';
			else $order_dir='DESC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `rating`.`create` DESC';

		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';

		/////WHERE
		$where='WHERE';
		if(isset($params["id"])) $where.=' `rating`.`id`='.DB::escapeInt($params["id"]).' AND';
		if(isset($params["ids"])) $where.=' `rating`.`id` IN ('.implode($params["ids"], ',').') AND';
		if(isset($params["type"])) $where.=' `rating`.`type`='.DB::escapeInt($params["type"]).' AND';
		else $where.=' `rating`.`type`=1 AND';
		if(isset($params["target_id"])) $where.=' `rating`.`target_id`='.DB::escapeInt($params["target_id"]).' AND';
		if(isset($params["user_id"])) $where.=' `rating`.`user_id`='.DB::escapeInt($params["user_id"]).' AND';

		if(strlen($where)>5) $where=rtrim($where, ' AND');
		else $where='';

		$q='SELECT COUNT(`id`) FROM `rating` '.$where;
		//die($q);
		$result["total_db"]=DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"]=0;
			return $result;
		}

		$q='SELECT 
				`rating`.*, DATE_FORMAT(`rating`.`create`, "%d&nbsp;%M %Y") AS `create_nice`,
				(SELECT `user`.`name` FROM `user` WHERE `user`.`id`=`rating`.`user_id` LIMIT 1) AS `user_name`
			FROM `rating`
			'.$where.'
			'.$order_by.'
			'.$limit;
		//die($q);

		$list=DB::getAll($q);
		if(!$list){
			$result["total"]=0;
			return $result;
		}

		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value!='') $result["list"]["$total"]["$name"]=$value;
				else $result["list"]["$total"]["$name"]='';
			}
			$total++;
		}
		$result["total"]=$total;

		//echo '<pre>';var_dump($result);
		return $result;
	}

	public static function delete($params) {

		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];

		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		DB::query('DELETE FROM `rating` WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::logging(5, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}

}