<?php if (!defined("INBOX")) die('separate call');

class User {

	const TOKEN_LIFETIME=5184000;//2 мес.

	protected static $_instance;

	private static $user;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	private static function user_data_default(){
		self::$user=array(
			"id"=>0,
			"ip"=>self::get_ip(),
			"name"=>'Гость',
			"phone"=>'',
			"email"=>'',
			"group_id"=>1,
			"group_name"=>'Гости',
			"permit"=>array()
		);
	}

	private static function user_data_auth($u){
		self::$user=array(
			"id"=>$u["id"],
			"name"=>$u["name"],
			"phone"=>$u["phone"],
			"email"=>$u["email"],
			"group_id"=>$u["group_id"],
			"group_name"=>$u["group_name"],
			"permit"=>$u["permit"]
		);
		if(isset($u["ip"])) self::$user["ip"]=$u["ip"];
		else self::$user["ip"]=self::get_ip();
	}

	private static function token_check($token_input){
		$token_input=self::token_parse($token_input);
		if(isset($token_input["error"])) return $token_input;
		
		if(!$token_row=DB::getRow('SELECT `id`, `user_id`, `token`, (CASE WHEN (`expire`>NOW()) THEN 0 ELSE 1 END) AS `expired` FROM `token` WHERE `id`=?i LIMIT 1', $token_input["id"])) return ["error"=> 'access token invalid'];

		if($token_input["str"]==$token_row["token"]) {
			if($token_row["expired"]==1) return array("error"=> 'token expired');

			$q="SELECT * FROM `user` WHERE `id`=?i LIMIT 1";
			if(!$u=DB::getRow($q, $token_row["user_id"])) return array("error"=> 'user not found ['.$token_input["user_id"].']');

			$row=DB::getRow('SELECT `name`, `permit` FROM `user_group` WHERE `id`=?i LIMIT 1', $u["group_id"]);
			$u["group_name"]=$row["name"];
			$u["permit"]=explode(',', $row["permit"]);

			self::user_data_auth($u);
			return true;

		}else return array("error"=> 'access token invalid');

	}

	private static function token_parse($t){
		if(mb_strlen($t, 'utf-8')<34 OR !strpos($t, ':')) return array("error"=> 'token is invalid. template "id:string" and more than 34 simbols ['.$t.']', 1);
		$t=explode(':', $t);
		if(!isset($t[1])) return array("error"=> 'unable to parse token');
		if((int)$t[0]<1 OR mb_strlen($t[1], 'utf-8')<32 OR mb_strlen($t[1], 'utf-8')>32) return array("error"=> 'unable to parse token. id['.$t[0].'] must be positive int, str['.$t[1].'] must be 32 length', 1);
		return array('id'=>$t[0], 'str'=>$t[1]);
	}

	private static function token_create(){

		$token["str"]=self::random_string(32);
		$token["expire"]=time()+self::TOKEN_LIFETIME;

		$q="INSERT INTO `token` (`user_id`, `token`, `create`, `expire`) VALUES (?i, ?s, NOW(), FROM_UNIXTIME(".$token["expire"]."))";
		if(!DB::query($q, self::id(), $token["str"])) return array("error"=> 'server token BD error');
		$token["id"]=DB::insertId();

		return $token;
	}

	private static function get_ip(){
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR'];
		elseif (!empty($_SERVER['REMOTE_ADDR'])) return $_SERVER['REMOTE_ADDR'];
		else return 'unknown';
	}


	public static function auth($token=false) {

		if (self::$_instance === null) {
			self::$_instance = new self;
			self::user_data_default();

			if($token) {
				self::token_check($token);
			}
		}
	}
	
	public static function auth_ulogin($var){

		$ulogin_data=file_get_contents('http://ulogin.ru/token.php?token='.$var["post"]["token"].'&host='.$_SERVER['HTTP_HOST']);
		$ulogin_data=json_decode($ulogin_data, true);

		if(empty($ulogin_data)) return ["error"=> 'ulogin_error: empty answer'];
		if(isset($ulogin_data["error"])) return ["error"=> 'ulogin_error: '.$ulogin_data["error"]];

		$user_id=false;
		//проверяем, существует ли такой пользователь в базе
		if($ulogin_db=DB::getRow('SELECT * FROM `ulogin` WHERE `ident`=?s LIMIT 1', $ulogin_data["identity"])){
			$user_id=DB::getOne('SELECT `id` FROM `user` WHERE `id`=?i LIMIT 1', $ulogin_db["user_id"]);
			if(!$user_id) DB::query('DELETE FROM `ulogin` WHERE `ident`=?s', $ulogin_data["identity"]);
			else $var["ulogin"]["password"]=substr($ulogin_data["identity"], 0, 8).$ulogin_db["salt"];
		}

		if(!$user_id){
			//добавляем нового пользователя
			$reg_user["name"]=$ulogin_data["first_name"].' '.$ulogin_data["last_name"];
			$reg_user["email"]=$ulogin_data["email"];
			$reg_user["salt"]=self::random_string(8);
			$reg_user["password"]=substr($ulogin_data["identity"], 0, 8).$reg_user["salt"];
			$reg_user["actkey"]=false;
			$reg_user["group_id"]=2;
			//добавляем пользователя в таблицу users сайта и получаем user_id новой записи
			$reg_user=self::create($reg_user);
			if(isset($reg_user["error"])){
				$var["error"]=$reg_user["error"];
				return $var;
			}

			if(!DB::query('INSERT INTO `ulogin` (`user_id`, `ident`, `salt`) VALUES (?i, ?s, ?s)', $reg_user["id"], $ulogin_data["identity"], $reg_user["salt"])){
				DB::query('DELETE FROM `user` WHERE `id`=?i LIMIT 1', $reg_user["id"]);
				$var["error"]=$reg_user["db ulogin error"];
				return $var;
			}
			
			$var["ulogin"]["password"]=substr($ulogin_data["identity"], 0, 8).$reg_user["salt"];

		}

		$var["ulogin"]["login"]=$ulogin_data["email"];
		return $var;
	}
	
	public static function login($login, $password){

		$q="
			SELECT `user`.*, `user_group`.`name` AS `group_name`, `user_group`.`permit`
			FROM `user`
			LEFT JOIN `user_group` ON `user_group`.`id`=`user`.`group_id`
			WHERE `email`=?s AND `actkey` IS NULL LIMIT 1";
		if(!$user=DB::getRow($q, $login)) return ["error"=> 'user not found'];
		if(!password_verify($password, $user["password"])) return ["error"=> 'password invalid'];

		self::user_data_auth(
			array(
				"id"=>$user["id"],
				"name"=>$user["name"],
				"phone"=>$user["phone"],
				"email"=>$user["email"],
				"group_id"=>$user["group_id"],
				"group_name"=>$user["group_name"],
				"permit"=>explode(',', $user["permit"])
			)
		);

		$token=self::token_create();

		if(!isset($token["error"])) SetCookie('data', $token["id"].':'.$token["str"], $token["expire"], '/');

		return true;

	}

	public static function logout(){
		self::user_data_default();
		//?
		return true;
	}

	public static function registration_start($var){
		if (!isset($var["post"]["user_name"]) OR !isset($var["post"]["user_password"]) OR !isset($var["post"]["user_password2"]) OR !isset($var["post"]["user_email"]) OR !isset($var["post"]["val_user_r"]) OR $var["post"]["val_user_r"]!='val_user_r_true') {
			$var["error"]='registration data error';
			return $var;
		}

		if($var["post"]["user_password"]!=$var["post"]["user_password2"]) {
			$var["error"]='password repeat';
			return $var;
		}

		$var["reg_user"]["name"]=strip_tags($var["post"]["user_name"]);
		$var["reg_user"]["email"]=strip_tags($var["post"]["user_email"]);
		$var["reg_user"]["phone"]=strip_tags($var["post"]["user_telephone"]);
		$var["reg_user"]["password"]=$var["post"]["user_password"];
		$var["reg_user"]["actkey"]=self::random_string(10);

		$var["reg_user"]=self::create($var["reg_user"]);
		if(isset($var["reg_user"]["error"])) {
			$var["error"]=$var["reg_user"]["error"];
			return $var;
		}

		if(!Notify::user_registration($var["reg_user"])) $var["error"]='notify error';

		return $var;
	}
	
	private static function create($user){
		
		if (strlen($user["password"])>16 OR strlen($user["password"])<8) {
			$user["error"]='password_length 8-16';
			return $user;
		}
		if (strlen($user["name"])>100 OR strlen($user["name"])<2) {
			$user["error"]='name_length';
			return $user;
		}
		if (strlen($user["email"])>50 OR strlen($user["email"])<8) {
			$user["error"]='email_length';
			return $user;
		}
		if (isset($user["phone"]) AND ($user["phone"]!='' AND(strlen($user["phone"])>25 OR strlen($user["phone"])<6))) {
			$user["error"]='phone_length';
			return $user;
		}else $user["phone"]='';
		if (!filter_var($user["email"], FILTER_VALIDATE_EMAIL)) {
			$user["error"]='email_wrong';
			return $user;
		}
		if (self::find_double_users($user["email"])) {
			$user["error"]='email_exist';
			return $user;
		}

		if(!isset($user["group_id"])) $user["group_id"]=1;

		$user["hash"]=password_hash($user["password"], PASSWORD_DEFAULT);
		
		if($user["actkey"]) {
			$q='INSERT INTO `user` (`name`, `group_id`, `password`, `phone`, `email`, `actkey`) VALUES (?s, ?i, ?s, ?s, ?s, ?s)';
			if(!DB::query($q, $user["name"], $user["group_id"], $user["hash"], $user["phone"], $user["email"], $user["actkey"])){
				$user["error"]='db error';
				return $user;
			}
		}else{
			$q='INSERT INTO `user` (`name`, `group_id`, `password`, `phone`, `email`, `actkey`) VALUES (?s, ?i, ?s, ?s, ?s, NULL)';
			if(!DB::query($q, $user["name"], $user["group_id"], $user["hash"], $user["phone"], $user["email"])){
				$user["error"]='db error';
				return $user;
			}
		}

		$user["id"]=DB::insertId();
		return $user;
	}
	
	public static function find_double_users($email){
		return DB::getOne('SELECT `id` FROM `user` WHERE `email`=?s', $email);
	}
	
	public static function email_activate($var) {

		if (!isset($var["get"]["u"]) OR !isset($var["get"]["k"]) OR strlen($var["get"]["u"])>10 OR strlen($var["get"]["k"])!=10) {
			$var["error"]='activation data error';
			return $var;
		}

		$q='UPDATE `user` SET `actkey`=NULL, `group_id`=2 WHERE `id`=?i AND `actkey`=?s LIMIT 1';
		if(DB::query($q, $var["get"]["u"], $var["get"]["k"]) AND DB::affectedRows()==1) {
			return $var;
		}else{
			$var["error"]='activation data db error';
			return $var;
		}
	}
	
	public static function logging($action_id, $target_id=0, $comment='') {
		if(DB::query(
			'INSERT INTO `user_log` (`user_id`, `action_id`, `target_id`, `comment`, `ip`) VALUES (?i, ?i, ?i, ?s, ?s)',
			self::id(), $action_id, $target_id, $comment, self::ip()
		)) return true;
		else return false;
	}

	public static function random_string($l=32){
		$s=str_shuffle('abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890');
		return substr($s, 1, $l);
	}

	public static function id() {
		return self::$user["id"];
	}
	public static function ip() {
		return self::$user["ip"];
	}
	public static function group_id() {
		return self::$user["group_id"];
	}
	public static function group_name() {
		return self::$user["group_name"];
	}
	public static function permit() {
		return self::$user["permit"];
	}
	public static function name() {
		return self::$user["name"];
	}
	public static function email() {
		return self::$user["email"];
	}
	public static function phone() {
		return self::$user["phone"];
	}
}