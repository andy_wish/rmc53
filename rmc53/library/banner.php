<?php if (!defined("INBOX")) die('separate call');

class Banner {

	public static function create($params) {
		$desc=[
			"image"=>[
				"type"=>'string',
				"require"=>true,
				"min"=>5,
				"max"=>255
			],
			"name"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"text"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"link"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"order"=>[
				"type"=>'int'
			],
			"type"=>[
				"type"=>'int'
			],
			"status"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["type"])) $params["type"]=1;
		if(!isset($params["name"])) $params["name"]='';
		if(!isset($params["text"])) $params["text"]='';
		if(!isset($params["order"])) $params["order"]=5;
		if(!isset($params["link"])) $params["link"]='';

		
		$q='INSERT INTO `banner` (`type`, `name`, `text`, `link`, `image`, `order`, `create`, `status`) VALUES (?i, ?s, ?s, ?s, ?s, ?i, NOW(), 1)';
		DB::query($q, $params["type"], $params["name"], $params["text"], $params["link"], $params["image"], $params["order"]);

		$id=DB::insertId();
		if($id) return array("id"=>$id);
		else return array('error'=>'db error');
	}

	public static function read($params) {

		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`banner`.`id`';
					break;
				case 'name':
					$order_by='`banner`.`name`';
					break;
				case 'order':
					$order_by='`banner`.`order`';
					break;
				case 'create':
					$order_by='`banner`.`create`';
					break;
				default:
					$order_by='`banner`.`order`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='desc') $order_dir='DESC';
			else $order_dir='ASC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `banner`.`order` ASC';

		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';

		/////WHERE
		$where='WHERE';

		if(isset($params["id"])) $where.=' `banner`.`id`='.DB::escapeInt($params["id"]).' AND';
		if(isset($params["type"])) $where.=' `banner`.`type`='.DB::escapeInt($params["type"]).' AND';
		if(isset($params["status"])) $where.=' `banner`.`status`='.DB::escapeInt($params["status"]).' AND';
		if(strlen($where)>5) $where=rtrim($where, ' AND');
		else $where='';

		$q='
			SELECT * FROM `banner`
			'.$where.'
			'.$order_by.'
			'.$limit;
		$list=DB::getAll($q);

		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value!='') $result["list"]["$total"]["$name"]=$value;
				else $result["list"]["$total"]["$name"]='';
			}
			$total++;
		}
		$result["total"]=$total;
		//echo '<pre>';var_dump($result);
		return $result;
	}

	public static function update($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"name"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"text"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"link"=>[
				"type"=>'string',
				"min"=>0,
				"max"=>255
			],
			"order"=>[
				"type"=>'int'
			],
			"status"=>[
				"type"=>'int',
				"min"=>0,
				"max"=>1
			],
			"image"=>[
				"type"=>'string',
				"min"=>5,
				"max"=>255
			],
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `banner` WHERE `id`=?i LIMIT 1', $params["id"])) return array('error'=>'banner '.$params["id"].' not found');

		//////SET
		$set='SET ';
		if(isset($params["name"])) $set.='`banner`.`name`='.DB::escapeString($params["name"]).', ';
		if(isset($params["text"])) $set.='`banner`.`text`='.DB::escapeString($params["text"]).', ';
		if(isset($params["link"])) $set.='`banner`.`link`='.DB::escapeString($params["link"]).', ';
		if(isset($params["order"])) $set.='`banner`.`order`='.DB::escapeInt($params["order"]).', ';
		if(isset($params["image"])) $set.='`banner`.`image`='.DB::escapeString($params["image"]).', ';
		if(isset($params["status"])) $set.='`banner`.`status`='.DB::escapeInt($params["status"]).', ';
		if(strlen($set)>7) $set.='`banner`.`create`=NOW()';
		else return array('error'=>'nothing to update');

		$q='UPDATE `banner` '.$set.' WHERE `banner`.`id`=?i LIMIT 1';
		if(DB::query($q, $params["id"])) return array("id"=>$params["id"]);
		else return array('error'=>'db error');
	}


}