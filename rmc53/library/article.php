<?php if (!defined("INBOX")) die('separate call');

class Article {

	public static function create($params) {
		$desc=[
			"name"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255,
				"require"=>true
			],
			"cat_id"=>[
				"type"=>'int',
				"require"=>true
			],
			"text"=>[
				"type"=>'html',
				"min"=>0
			],
			"name_translit"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"description"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"keywords"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["name_translit"])) $params["name_translit"]=Core::translit($params["name"]);
		if(!isset($params["text"])) $params["text"]='';
		$q="
			INSERT INTO `article` (`cat_id`, `name`, `name_translit`, `text`, `author_id`)
			VALUES (?i, ?s, ?s, ?s, ?i)
		";
		DB::query($q, $params["cat_id"], $params["name"], $params["name_translit"], $params["text"], User::id());
		$id=DB::insertId();
		if($id) return array("id"=>$id);
		else return array('error'=>'db error');
		
		return $result;
	}

	public static function read($params) {

		if(!isset($params["response"]["type"])) $params["response"]["type"]='list';
		if(!isset($params["response"]["text_preview"])) $params["response"]["text_preview"]=255;
		if(!isset($params["response"]["status"])) $params["response"]["status"]='active';

		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`article`.`id`';
					break;
				case 'name':
					$order_by='`article`.`name`';
					break;
				case 'author_id':
					$order_by='`article`.`author_id`';
					break;
				case 'create':
					$order_by='`article`.`create`';
					break;
				default:
					$order_by='`article`.`create`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='asc') $order_dir='ASC';
			else $order_dir='DESC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `article`.`create` DESC';

		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';

		/////WHERE
		$where='WHERE';

		if(isset($params["id"])) $where.=' `article`.`id`='.DB::escapeInt($params["id"]).' AND';
		if(isset($params["ids"])) $where.=' `article`.`id` IN ('.implode($params["ids"], ',').') AND';
		if(isset($params["cat_id"])) $where.=' `article`.`cat_id`='.DB::escapeInt($params["cat_id"]).' AND';
		if(isset($params["name_translit"])) $where.=' `article`.`name_translit`='.DB::escapeString($params["name_translit"]).' AND';
		if(isset($params["author_id"])) $where.=' `article`.`author_id`='.DB::escapeInt($params["author_id"]).' AND';
		if(isset($params["search_str"])) $where.=' (`article`.`name` LIKE('.DB::escapeString('%'.$params["search_str"].'%').') OR `article`.`text` LIKE('.DB::escapeString('%'.$params["search_str"].'%').')) AND';

		/////SELECT
		$select='
			SELECT 
				`article`.*, DATE_FORMAT(`article`.`create`, "%d&nbsp;%M %Y") AS `create_nice`,
		
				`cat`.`name` AS `cat_name`, `cat`.`name_translit` AS `cat_name_translit`,
				`cat_parent`.`id` AS `cat_id_parent`, `cat_parent`.`name` AS `cat_name_parent`, `cat_parent`.`name_translit` AS `cat_name_translit_parent`,
		
				(SELECT `user`.`name` FROM `user` WHERE `user`.`id`=`article`.`author_id` LIMIT 1) AS `author_name`,
				(SELECT COUNT(`comment`.`id`) FROM `comment` WHERE `comment`.`type`=1 AND `comment`.`target_id`=`article`.`id`) AS `comment_count`,
				(SELECT FORMAT(AVG(`rating`.`score`), 1) FROM `rating` WHERE `rating`.`type`=1 AND `rating`.`target_id`=`article`.`id`) AS `rating`,
				(SELECT `file`.`src` FROM `file` WHERE `file`.`article_id`=`article`.`id` AND `file`.`type`="image" ORDER BY `file`.`order` LIMIT 1) AS `img_src`';

		switch ($params["response"]["type"]){

			case 'list':
				$select.=', (SELECT COUNT(`file`.`id`) FROM `file` WHERE `file`.`article_id`=`article`.`id`) AS `file_count`';
				break;

			case 'read':
				DB::query('SET @@session.group_concat_max_len = 10000;');
				$select.=', (SELECT GROUP_CONCAT(DISTINCT CONCAT_WS("}~f{", `file`.`id`, `file`.`type`, `file`.`src`, `file`.`order`, `file`.`name`, `file`.`desc`) SEPARATOR "}~next{") FROM `file` WHERE `file`.`article_id`=`article`.`id` GROUP BY `article_id`) AS `files`';
				break;

		}
		switch ($params["response"]["status"]){
			case 'active':
				$where.=' `article`.`close` IS NULL AND';
				break;
			case 'all':
				break;
		}

		if(strlen($where)>5) $where=rtrim($where, ' AND');
		else $where='';

		$q='SELECT COUNT(`id`) FROM `article` '.$where;
		//die($q);
		$result["total_db"]=DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"]=0;
			return $result;
		}
		

		$q=$select.'
			FROM `article`
			LEFT JOIN `category` AS `cat` ON `cat`.`id`=`article`.`cat_id`
			LEFT JOIN `category` AS `cat_parent` ON `cat_parent`.`id`=`cat`.`id_parent`
			'.$where.'
			'.$order_by.'
			'.$limit;
		//die($q);
		$list=DB::getAll($q);


		if(!$list){
			$result["total"]=0;
			return $result;
		}
		
		$id_current=false;
		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value=='') {
					$result["list"]["$total"]["$name"]='';
					continue;
				}
				
				if($name=='files'){
					$files=explode('}~next{', $value);
					foreach($files as $file){
						$f=explode('}~f{', $file);
						if($f[1]=='image') $file_arr_name='images';
						else $file_arr_name='files';

						$f_arr=array(
							"id"=>$f[0],
							"type"=>$f[1],
							"src"=>$f[2],
							"order"=>$f[3]
						);
						if(isset($f[4])) $f_arr["name"]=$f[4];
						else $f_arr["name"]='';
						if(isset($f[5])) $f_arr["desc"]=$f[5];
						else $f_arr["desc"]='';

						$result["list"]["$total"]["$file_arr_name"][]=$f_arr;
					}

					continue;
				}
				
				if($name=='text'){
					$result["list"]["$total"]["$name"]=htmlspecialchars_decode($value);
					if(
						$params["response"]["type"]=='list'
						AND strlen($result["list"]["$total"]["$name"])>$params["response"]["text_preview"]
					){
						$result["list"]["$total"]["$name"]=mb_substr(strip_tags($result["list"]["$total"]["$name"]), 0, $params["response"]["text_preview"]).'...';
					}
					continue;
				}

				$result["list"]["$total"]["$name"]=$value;
			}
			$total++;
		}
		$result["total"]=$total;

		//echo '<pre>';var_dump($result);
		return $result;
	}

	public static function update($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"name"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"text"=>[
				"type"=>'html'
			],
			"cat_id"=>[
				"type"=>'int'
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `article` WHERE `id`=?i LIMIT 1', $params["id"])) return array('error'=>'article ['.$params["id"].'] not found');

		//////SET
		$set=array();
		if(isset($params["name"])) $set[]=DB::parse('`article`.`name`=?s, `article`.`name_translit`=?s', $params["name"], Core::translit($params["name"]));
		if(isset($params["text"])) $set[]=DB::parse('`article`.`text`=?s', $params["text"]);
		if(isset($params["cat_id"])) $set[]=DB::parse('`article`.`cat_id`=?i', $params["cat_id"]);
		if(count($set)) $set='SET '.implode(', ', $set);
		else return ['error'=>'nothing to update'];

		if(DB::query('UPDATE `article` ?p WHERE `article`.`id`=?i LIMIT 1', $set, $params["id"])) return array("id"=>$params["id"]);
		else return array('error'=>'db error');
	}

	public static function delete($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		DB::query('UPDATE `article` SET `close`=NOW() WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::logging(3, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}

	public static function restore($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		DB::query('UPDATE `article` SET `close`=NULL WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::logging(4, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}

}