<?php if (!defined("INBOX")) die('separate call');

class Access {

	protected static $_instance;
	protected static $access_list;
	protected static $group_id;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	public static function init($list) {
		if (self::$_instance === null) {
			self::$_instance = new self;
			self::$access_list=$list;
			self::$group_id=User::group_id();
		}
	}

	public static function permit($controller, $action){

		if(!isset(self::$access_list["$controller"])) {
			if(in_array('all', self::$access_list["default"])) return true;
			if(in_array(self::$group_id, self::$access_list["default"])) return true;
			return false;
		}

		if(!isset(self::$access_list["$controller"]["$action"])) {
			if(in_array('all', self::$access_list["$controller"]["default"])) return true;
			if(in_array(self::$group_id, self::$access_list["$controller"]["default"])) return true;
			return false;
		}

		if(in_array('all', self::$access_list["$controller"]["$action"])) return true;
		if(in_array(self::$group_id, self::$access_list["$controller"]["$action"])) return true;
		return false;
	}
	
}