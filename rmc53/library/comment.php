<?php if (!defined("INBOX")) die('separate call');

class Comment {

	public static function create($params) {

		$desc=[
			"target_id"=>[
				"type"=>'int',
				"min"=>0,
				"require"=>true
			],
			"type"=>[
				"type"=>'int',
				"require"=>true
			],
			"text"=>[
				"type"=>'string',
				"max"=>100,
				"require"=>true
			]
		];

		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		$q='
			INSERT INTO `comment` (`user_id`, `type`, `target_id`, `ip`, `text`)
			VALUES (?i, ?i, ?i, ?s, ?s)
		';
		DB::query($q, User::id(), $params["type"], $params["target_id"], User::ip(), $params["text"]);
		$id=DB::insertId();
		if($id) return array("id"=>$id);
		else return array('error'=>'db error');
		
		return $result;
	}

	public static function read($params) {
		$desc=[
			"id"=>[
				"type"=>'int'
			],
			"ids"=>[
				"type"=>'array_of_int'
			],
			"target_id"=>[
				"type"=>'int',
				"min"=>0
			],
			"user_id"=>[
				"type"=>'int'
			],
			"type"=>[
				"type"=>'int',
				"max"=>100
			],
			"search_str"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>32
			],
			"response"=>[
				"type"=>'helper'
			]
		];

		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`comment`.`id`';
					break;
				case 'user_name':
					$order_by='`user_name`';
					break;
				case 'create':
					$order_by='`comment`.`create`';
					break;
				default:
					$order_by='`comment`.`create`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='asc') $order_dir='ASC';
			else $order_dir='DESC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `comment`.`create` DESC';

		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';

		/////WHERE
		$where='WHERE';
		if(isset($params["id"])) $where.=' `comment`.`id`='.DB::escapeInt($params["id"]).' AND';
		if(isset($params["ids"])) $where.=' `comment`.`id` IN ('.implode($params["ids"], ',').') AND';
		if(isset($params["type"])) $where.=' `comment`.`type`='.DB::escapeInt($params["type"]).' AND';
		//else $where.=' `comment`.`type`=1 AND';
		if(isset($params["target_id"])) $where.=' `comment`.`target_id`='.DB::escapeInt($params["target_id"]).' AND';
		if(isset($params["user_id"])) $where.=' `comment`.`user_id`='.DB::escapeInt($params["user_id"]).' AND';
		if(isset($params["search_str"])) $where.=' (`comment`.`name` LIKE('.DB::escapeString('%'.$params["search_str"].'%').') OR `user`.`name` LIKE('.DB::escapeString('%'.$params["search_str"].'%').') OR `comment`.`text` LIKE('.DB::escapeString('%'.$params["search_str"].'%').')) AND';
		if(strlen($where)>5) $where=rtrim($where, ' AND');
		else $where='';

		$q='SELECT COUNT(`id`) FROM `comment` '.$where;
		//die($q);
		$result["total_db"]=DB::getOne($q);
		if($result["total_db"]==0){
			$result["total"]=0;
			return $result;
		}

		$q='SELECT 
				`comment`.*, DATE_FORMAT(`comment`.`create`, "%d&nbsp;%M %Y") AS `create_nice`,
				(SELECT `user`.`name` FROM `user` WHERE `user`.`id`=`comment`.`user_id` LIMIT 1) AS `user_name`,
				(SELECT `comment_type`.`name` FROM `comment_type` WHERE `comment_type`.`id`=`comment`.`type` LIMIT 1) AS `type_name`
			FROM `comment`
			'.$where.'
			'.$order_by.'
			'.$limit;
		//die($q);

		$list=DB::getAll($q);
		if(!$list){
			$result["total"]=0;
			return $result;
		}

		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value!='') $result["list"]["$total"]["$name"]=$value;
				else $result["list"]["$total"]["$name"]='';
			}
			$total++;
		}
		$result["total"]=$total;

		//echo '<pre>';var_dump($result);
		return $result;
	}

	public static function delete($params) {

		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];

		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		DB::query('DELETE FROM `comment` WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::logging(5, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}

}