<?php if (!defined("INBOX")) die('separate call');

class Category {

	public static function read($params) {
		//echo '<pre>';var_dump($params);exit;
		if(!isset($params["response"]["type"])) $params["response"]["type"]='all';

		switch($params["response"]["type"]){
			case 'all':
				$q='
					SELECT `category`.*,
					`sub_cat`.`id` AS `sub_id`, `sub_cat`.`name` AS `sub_name`, `sub_cat`.`name_translit` AS `sub_name_translit`, `sub_cat`.`desc` AS `sub_desc`, `sub_cat`.`order` AS `sub_order`, `sub_cat`.`popular` AS `sub_popular`, `sub_cat`.`type` AS `sub_type`,
					(SELECT COUNT(`article`.`id`) FROM `article` WHERE `article`.`cat_id`=`sub_cat`.`id`) AS `sub_article_count`
					FROM `category`
					LEFT JOIN `category` AS `sub_cat` ON `category`.`id`=`sub_cat`.`id_parent`
					WHERE `category`.`id_parent`=0
					ORDER BY `category`.`order`, `sub_cat`.`order`';
				$list=DB::getAll($q);

				$result=array();
				$cat_id_current=false;
				$i=0;
				$total=-1;
				foreach($list as $row){
					if($row["id"]!=$cat_id_current){
						$cat_id_current=$row["id"];
						$total++;
						$i=0;
						foreach($row as $name=>$value){
							if(substr($name, 0, 4)=='sub_') continue;
							if($value!='') $result["$total"]["$name"]=$value;
							else $result["$total"]["$name"]='';
						}
						if($row["sub_id"]=='') {
							$result["$total"]["sub"]='';
							continue;
						}

					}
					foreach($row as $name=>$value){
						if(substr($name, 0, 4)!='sub_') continue;
						if($value!='') $result["$total"]["sub"]["$i"]["$name"]=$value;
						else $result["$total"]["sub"]["$i"]["$name"]='';
					}
					$i++;
				}
				break;
				
			case 'sub_cat':
				
				break;
			
			case 'one':
				if(!isset($params["id"]) AND !isset($params["name_translit"]) AND !isset($params["name"])) return array("error"=> 'id/name_translit/name required');
				if(isset($params["id"])) $where='`category`.`id`='.DB::escapeInt($params["id"]);
				elseif(isset($params["name_translit"])) $where='`category`.`name_translit`='.DB::escapeString($params["name_translit"]);
				elseif(isset($params["name"])) $where='`category`.`name`='.DB::escapeString($params["name"]);
				$result=DB::getRow('SELECT * FROM `category` WHERE '.$where.' LIMIT 1');
				break;
		}

		//echo '<pre>';var_dump($result);
		return $result;
	}


}