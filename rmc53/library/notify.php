<?php if (!defined("INBOX")) die('separate call');

Notify::instance();

class Notify {

	protected static $_instance;
	protected static $config;

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){
		require ROOT_DIR.'/config/notify.php';
		self::$config=$conf["notify"];
		require ROOT_DIR.'/plugins/send_mail_smtp_class.php';
	}

	public static function instance() {
		if (self::$_instance === null) {
			self::$_instance = new self;
		}
	}

	public static function user_registration($user) {
		$subject='Добро пожаловать!';
		$mail_text='
			<p>'.$user["name"].', сохраните это сообщение. Ваша учётная запись:<br />
			Логин: '.$user["email"].'<br />
			Пароль: '.$user["password"].'<br />
			<a href="http://'.$_SERVER['SERVER_NAME'].'/user/activate/?u='.$user["id"].'&k='.$user["actkey"].'" style="
				background-color: #38d;
				border-radius: 5px;
				color: #eee;
				display: block;
				padding: 10px;
				margin-top:5px;
				text-align: center;
				text-decoration: none;
				width: 120px;" target="_blank" rel=" noopener noreferrer">Активировать</a>
			</p>
			<p style="padding:24px 0 5px;line-height:1.2;font-size:12px;color:#555;">Если кнопка не работает, скопируйте и вставьте эту ссылку в адресную строку браузера:<br />
			http://'.$_SERVER['SERVER_NAME'].'/user/activate/?u='.$user["id"].'&k='.$user["actkey"].'
			</p>
		';
		self::email($user["email"], $mail_text, $subject) or die('register mail send error');
		return true;
	}
	
	public static function feedback($params) {

		$subject='Обратная связь rmc53.ru';
		$mail_text='
			<p>'.$params["name"].'['.User::id().']['.User::ip().']<br />
			email: <a href="mailto:'.$params["email"].'?subject=Ответ на ваше обращение на rmc53.ru">'.$params["email"].'</a><br />
		';
		if(isset($params["phone"]) AND $params["phone"]!='') $mail_text.='телефон: '.$params["phone"].'<br />';
		$mail_text.='</p>';

		$mail_text.='<p style="padding:10px;">'.$params["text"].'</p>';

		foreach(self::$config["feedback_target"] as $feedback_target){
			if(!self::email($feedback_target, $mail_text, $subject)) return ["error"=>'mail send error'];
		}
		return true;
	}
	
	private static function email($target, $text, $subject='оповещение') {
		//require_once(ROOT_DIR.'/plugins/send_mail_smtp_class.php');

		$mailSMTP=new SendMailSmtpClass(
			self::$config["smtp"]["username"],
			self::$config["smtp"]["password"],
			self::$config["smtp"]["host"],
			self::$config["name_from"],
			self::$config["smtp"]["port"]
		);

		// заголовок письма
		$headers="MIME-Version: 1.0\r\n";
		$headers.="Content-type: text/html; charset=".self::$config["charset"]."\r\n"; // кодировка письма
		$headers.="From: ".self::$config["name_from"]." <".self::$config["email_from"].">\r\n"; // от кого письмо
		$result=$mailSMTP->send($target, $subject, $text, $headers);
		
		if($result === true) return true;
		else return false;
	}

}