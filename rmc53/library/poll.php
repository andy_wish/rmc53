<?php if (!defined("INBOX")) die('separate call');

class Poll {

	const DEFAULT_HTML_STYLE='bootstrap4';

	public static function read_list($params) {

		$q='
		SELECT *,
			DATE_FORMAT(`start`, "%d&nbsp;%M %Y") AS `start_nice`,
			DATE_FORMAT(`finish`, "%d&nbsp;%M %Y") AS `finish_nice`,
			(CASE WHEN (`finish`<NOW()) THEN 1 ELSE 0 END) AS `finished`,
			(SELECT COUNT(`poll_fill`.`id`) FROM `poll_fill` WHERE `poll_fill`.`poll_id`=`poll`.`id` GROUP BY `poll_fill`.`created`) AS `fill_count`
		FROM `poll`
		ORDER BY `id` DESC
		LIMIT 100';
		if(!$list=DB::getAll($q)) {
			$result["total"]=0;
			return $result;
		}

		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value!='') $result["list"]["$total"]["$name"]=$value;
				else $result["list"]["$total"]["$name"]='';
			}
			$total++;
		}
		$result["total"]=$total;

		//echo '<pre>';var_dump($result);
		return $result;
	}
	
	public static function read($params) {

		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"style"=>[
				"type"=>'string'
			]
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["style"])) $params["style"]=self::DEFAULT_HTML_STYLE;

		if(!$result=DB::getRow('SELECT *, (CASE WHEN (`finish`<NOW()) THEN 1 ELSE 0 END) AS `finished` FROM `poll` WHERE `id`=?i LIMIT 1', $params["id"])) return ["error"=>'not found['.$params["id"].']'];

		if($result["finished"]) return ["error"=>'опрос окончен'];

		$q='SELECT 
				`poll_field`.*,
				`poll_field_type`.`name` AS `type_name`
			FROM `poll_field`
			
			LEFT JOIN `poll_field_type` ON `poll_field_type`.`id`=`poll_field`.`type_id`
			WHERE `poll_field`.`poll_id`=?i
			ORDER BY `poll_field`.`order_num` ASC
			
			';
		//die($q);

		$list=DB::getAll($q, $params["id"]);
		if(!$list){
			$result["total"]=0;
			return $result;
		}

		$result["html"]='';

		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value!='') $result["list"]["$total"]["$name"]=$value;
				else $result["list"]["$total"]["$name"]='';
			}
			$result["html"].=self::field_to_html($row, $params["style"]);
			$total++;
		}
		$result["total"]=$total;
		
		$result["html"]=self::result_html($result, $params["style"]);

		//echo '<pre>';var_dump($result);
		return $result;
	}
	
	public static function read_filled($params) {

		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		if(!$result["details"]=DB::getRow('SELECT *, (CASE WHEN (`finish`<NOW()) THEN 1 ELSE 0 END) AS `finished` FROM `poll` WHERE `id`=?i LIMIT 1', $params["id"])) return ["error"=>'poll not found['.$params["id"].']'];

		if(!$result["fields"]=DB::getAll('SELECT * FROM `poll_field` WHERE `poll_id`=?i ORDER BY `order_num` ASC', $params["id"])) return ["error"=>'fields not found['.$params["id"].']'];

		$q='SELECT *, DATE_FORMAT(`created`, "%d.%m.%y %H:%i") AS `created_nice`
			FROM `poll_fill`
			WHERE `poll_id`=?i
			ORDER BY `created` DESC, `user_tag`, `field_id` ASC, `filler`
			LIMIT 10000
		';
		//die($q);

		$list=DB::getAll($q, $params["id"]);
		if(!$list){
			$result["total"]=0;
			return $result;
		}


		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value!='') $result["list"]["$total"]["$name"]=$value;
				else $result["list"]["$total"]["$name"]='';
			}
			$total++;
		}
		$result["total"]=$total;

		//echo '<pre>';var_dump($result);
		return $result;
	}

	public static function fill($params) {

		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"start"=>[
				"type"=>'int',
				"require"=>true,
				"min"=>1537422438,
				"max"=>3000000000
			],
			"tag"=>[
				"type"=>'string',
				"min"=>10,
				"max"=>32
			],
			"fields"=>[
				"type"=>'not_verify',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		if(!isset($params["tag"])) $params["tag"]=$params["start"];
		$values=[];
		foreach ($params["fields"] as $field){
			$values[]=DB::parse('(?i, ?i, ?s, '.User::id().', ?s, \''.User::ip().'\')', $params["id"], $field["id"], $params["tag"], $field["value"]);
		}
		if(count($values)) $values='VALUES '.implode(', ', $values);
		else return ['error'=>'nothing to fill'];
		
		$q='
			INSERT INTO `poll_fill` (`poll_id`, `field_id`, `user_tag`, `user_id`, `filler`, `ip`)
			'.$values;
		//die($q);

		DB::query($q);
		if($total=DB::affectedRows()) return array("total"=>$total);
		else return array('error'=>'db error');

	}

	public static function delete($params) {

		$params=Verify::filter($params, [
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		]);
		if(isset($params["error"])) return $params;

		DB::query('UPDATE `poll` SET `finish`=NOW() WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::logging(20, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}


	private static function result_html($result, $style=self::DEFAULT_HTML_STYLE){

		switch ($style){
			case 'bootstrap4':
				return '<form action="http://rmc53.ru/poll/fill/" enctype="multipart/form-data" method="post" id="poll_'.$result["id"].'" name="poll_'.$result["id"].'" style="font-family: Tahoma, Geneva, sans-serif;margin:1rem 0;border-radius: 0.26rem;padding: 1rem 2rem;background: #fff;box-shadow: 0 0 15px rgba(0, 0, 0, 0.3);color:#111;">
	<input type="hidden" name="poll_id" value="'.$result["id"].'">
	<input type="hidden" name="s" value="">
	<input type="hidden" name="t" value="">
		'.$result["html"].'
	<button type="submit" class="btn btn-primary">Отправить</button>
</form>';
				break;
		}
		
		return 'poll html error';
	}

	private static function field_to_html($field, $style=self::DEFAULT_HTML_STYLE){

		switch ($style){
			case 'bootstrap4':
				
				switch ($field["type_name"]){
					case 'select_one':
					case 'select_multiple':
					case 'select_image':
					case 'select_images':
						
						break;

					case 'text_short':
						return '
<div class="form-group" id="field_container_'.$field["id"].'">
	<label for="field_input_'.$field["id"].'">'.$field["name"].'</label>
	<input type="text" class="form-control" name="field_'.$field["id"].'" value="" id="field_input_'.$field["id"].'" autocomplete="on" maxlength="255" minlength="3" required>
</div>';
						break;

					case 'text_long':
						return '
<div class="form-group" id="field_container_'.$field["id"].'">
	<label for="field_input_'.$field["id"].'">'.$field["name"].'</label>
	<textarea class="form-control" rows="2" name="field_'.$field["id"].'" id="field_input_'.$field["id"].'"></textarea>
</div>';
						break;

					case 'upload_file':
						return '
<div class="form-group" id="field_container_'.$field["id"].'">
	<label for="field_input_'.$field["id"].'">'.$field["name"].'</label>
	<input type="file" class="form-control-file" name="field_'.$field["id"].'" id="field_input_'.$field["id"].'">
</div>';
						break;

					case 'number':
					case 'dropdown':
					case 'scale':
					case 'rating_star':
					case 'rank':
					case 'fio':
					case 'fio_granular':
					case 'address':
					case 'address_granular':
					case 'login':
					case 'password':
					case 'email':
					case 'phone':
					case 'date':
					case 'time':
					case 'date_time':
					case 'point_distribute':
					case 'html':
					
						break;
				}
			break;
		}
		return 'field_html error';
	}
	
}