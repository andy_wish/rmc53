<?php if (!defined("INBOX")) die('separate call');

class Page {

	public static function create($params) {
		$desc=[
			"name"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255,
				"require"=>true
			],
			"text"=>[
				"type"=>'html',
				"require"=>true
			],
			"name_translit"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"description"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"keywords"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			]
		];

		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!isset($params["name_translit"])) $params["name_translit"]=Core::translit($params["name"]);
		if(!isset($params["description"])) $params["description"]=$params["name"];
		if(!isset($params["keywords"])) $params["keywords"]=$params["name"];

		$q="
			INSERT INTO `page` (`name`, `name_translit`, `text`, `description`, `keywords`)
			VALUES (?s, ?s, ?s, ?s, ?s)
		";

		DB::query($q, $params["name"], $params["name_translit"], $params["text"], $params["description"], $params["keywords"]);
		$id=DB::insertId();
		if($id) return array("id"=>$id);
		else return array('error'=>'db error');

		return $result;
	}

	public static function read($params) {
		$desc=[
			"name_translit"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"id"=>[
				"type"=>'int'
			],
			"ids"=>[
				"type"=>'array_of_int'
			],
			"search_str"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"response"=>[
				"type"=>'helper'
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		/////ORDER BY
		if(isset($params["response"]["order"])){
			switch ($params["response"]["order"]) {
				case 'id':
					$order_by='`page`.`id`';
					break;
				case 'name':
					$order_by='`page`.`name`';
					break;
				case 'created':
					$order_by='`page`.`created`';
					break;
				default:
					$order_by='`page`.`created`';
			}
			if(isset($params["response"]["direction"]) AND $params["response"]["direction"]=='asc') $order_dir='ASC';
			else $order_dir='DESC';
			$order_by='ORDER BY '.$order_by.' '.$order_dir;
		}else $order_by='ORDER BY `page`.`created` DESC';

		/////LIMIT
		if(isset($params["response"]["limit"])){
			if(!isset($params["response"]["offset"])) $limit='LIMIT '.DB::escapeInt($params["response"]["limit"]);
			else $limit='LIMIT '.DB::escapeInt($params["response"]["offset"]).', '.DB::escapeInt($params["response"]["limit"]);
		}else $limit='LIMIT 20';

		/////WHERE
		$where='WHERE';
		if(isset($params["id"])) $where.=' `page`.`id`='.DB::escapeInt($params["id"]).' AND';
		if(isset($params["ids"])) $where.=' `page`.`id` IN ('.implode($params["ids"], ',').') AND';
		if(isset($params["name_translit"])) $where.=' `page`.`name_translit`='.DB::escapeString($params["name_translit"]).' AND';
		if(isset($params["search_str"])) $where.=' (`page`.`name` LIKE('.DB::escapeString('%'.$params["search_str"].'%').') OR `page`.`text` LIKE('.DB::escapeString('%'.$params["search_str"].'%').')) AND';

		switch ($params["response"]["status"]){
			case 'active':
				$where.=' `page`.`closed` IS NULL AND';
				break;
			case 'all':
				break;
		}

		if(strlen($where)>5) $where=rtrim($where, ' AND');
		else $where='';

		$q='
			SELECT 
				`page`.*, DATE_FORMAT(`page`.`created`, "%d&nbsp;%M %Y") AS `created_nice`,
				(SELECT COUNT(`comment`.`id`) FROM `comment` WHERE `comment`.`type`=2 AND `comment`.`target_id`=`page`.`id`) AS `comment_count`,
				(SELECT FORMAT(AVG(`rating`.`score`), 1) FROM `rating` WHERE `rating`.`type`=2 AND `rating`.`target_id`=`page`.`id`) AS `rating`
			FROM `page`
			'.$where.'
			'.$order_by.'
			'.$limit;
		//die($q);
		$list=DB::getAll($q);


		if(!$list){
			$result["total"]=0;
			return $result;
		}
		
		$total=0;
		foreach($list as $row){
			foreach($row as $name=>$value){
				if($value=='') {
					$result["list"]["$total"]["$name"]='';
					continue;
				}
				
				if($name=='text'){
					$result["list"]["$total"]["$name"]=htmlspecialchars_decode($value);
					continue;
				}

				$result["list"]["$total"]["$name"]=$value;
			}
			$total++;
		}
		$result["total"]=$total;

		//echo '<pre>';var_dump($result);
		return $result;
	}

	public static function update($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			],
			"name"=>[
				"type"=>'string',
				"min"=>3,
				"max"=>255
			],
			"text"=>[
				"type"=>'html'
			],
			"cat_id"=>[
				"type"=>'int'
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		if(!DB::getOne('SELECT `id` FROM `page` WHERE `id`=?i LIMIT 1', $params["id"])) return array('error'=>'page '.$params["id"].' not found');

		//////SET
		$set=array();
		if(isset($params["name"])) $set[]=DB::parse('`page`.`name`=?s, `page`.`name_translit`=?s', $params["name"], Core::translit($params["name"]));
		if(isset($params["text"])) $set[]=DB::parse('`page`.`text`=?s', $params["text"]);
		if(isset($params["cat_id"])) $set[]=DB::parse('`page`.`cat_id`=?i', $params["cat_id"]);
		if(count($set)) $set='SET '.implode(', ', $set);
		else return ['error'=>'nothing to update'];

		if(DB::query('UPDATE `page` ?p WHERE `page`.`id`=?i LIMIT 1', $set, $params["id"])) return array("id"=>$params["id"]);
		else return array('error'=>'db error');
	}

	public static function delete($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		DB::query('UPDATE `page` SET `close`=NOW() WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::user_log(3, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}

	public static function restore($params) {
		$desc=[
			"id"=>[
				"type"=>'int',
				"require"=>true
			]
		];
		$params=Verify::filter($params, $desc);
		if(isset($params["error"])) return $params;

		DB::query('UPDATE `page` SET `close`=NULL WHERE `id`=?i LIMIT 1', $params["id"]);
		if(DB::affectedRows()) {
			User::user_log(4, $params["id"]);
			return array("id"=>$params["id"]);
		}else return array('error'=>'not found');
	}

}