<?php
class JsonRPCRequest{

	const JSON_RPC_VERSION = '2.0';
	private $url;
	private $access_token;

	public function __construct($url, $access_token){
		$this->url=$url;
		$this->access_token=$access_token;
	}
	
	public function request($method, $params, $id=null){
		$params["access_token"]=$this->access_token;
		$data = array(
			"jsonrpc" => self::JSON_RPC_VERSION,
			"method" => $method,
			"params"=> $params
		);
		if(isset($id)) $data["id"]=$id;

		$ch = curl_init($this->url);
		curl_setopt_array($ch, array(
			CURLOPT_POST => TRUE,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
			CURLOPT_POSTFIELDS => json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)
			//CURLOPT_POSTFIELDS => $data
		));

		$response = curl_exec($ch);
		if($response === FALSE){
			die(curl_error($ch));
		}
		return $response;
	}

	public function link_prepare_files($files){
		$i=0;
		foreach($files as $file){
			//$data["$i"]["type"]=mime_content_type($file); //MIME-type, image/jpeg, image/png на данный момент доступны
			$data["$i"]["type"]='image/jpeg';
			//$data["$i"]["desc"]='описание, не обязательно';
			$data["$i"]["link"]=$file;
			$i++;
		}
		return $data;
	}

	public function base64_encode_files($files){
		$i=0;
		foreach($files as $file){
			$img_bin=fread(fopen($file, "r"), filesize($file));
			$img_str=base64_encode($img_bin);
			$data["$i"]["type"]=mime_content_type($file); //MIME-type, image/jpeg, image/png на данный момент доступны
			//$data["$i"]["desc"]='описание картинки, не обязательно';
			$data["$i"]["data"]=$img_str;
			$i++;
		}
		return $data;
	}

}